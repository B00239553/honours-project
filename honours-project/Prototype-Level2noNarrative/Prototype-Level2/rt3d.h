//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#ifndef RT3D
#define RT3D

#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <map>
#include <fstream>

#define RT3D_VERTEX   0
#define RT3D_NORMAL   1
#define RT3D_TEXCOORD 2
#define RT3D_INDEX    3
#define RT3D_TANGENT  4

/*
* Library used for initialising shaders, setting shader variables, creating
* and drawing meshes. Based on rt3d library created by Daniel Livingstone.
*/
namespace rt3d
{
	struct lightStruct 
	{
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];		
		GLfloat spotCosCutOff;
		GLfloat spotExponent;
	};

	struct materialStruct
	{
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat shininess;
	};

	char* loadFile(const char *fileName, GLint &fileSize);
	void shaderError(const GLint shader);
	GLuint initShaders(const char *vertFile, const char *fragFile);	
	void setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setUniformVector3fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setUniformVector4fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setLight(const GLuint program, const lightStruct light);
	void setMaterial(const GLuint program, const materialStruct material);
	GLuint createMesh(const GLuint numVerts, const GLfloat *vertices, const GLfloat *normals, const GLfloat *texCoords, 
		const GLuint indexCount, const GLuint * indices, const GLfloat *tangents);
	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size);
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);
};

#endif