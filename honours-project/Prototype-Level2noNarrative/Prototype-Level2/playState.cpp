//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "game.h"
#include "playState.h"

/*
* Constructor.
*/
playState::playState(void)
{
}

/*
* Variable Constructor.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for objects.
* @param - GLuint - shader program for HUD.
*/
playState::playState(GLuint mesh, GLuint count, GLuint objShader, GLuint hShader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = objShader;
	hudShader = hShader;	
	attenuation = glm::vec3(1.0f, 0.0f, 0.005f);
}

/*
* Deconstructor.
*/
playState::~playState(void)
{
}

/*
* 
* @param - game - the context of the game.
*/
void playState::init(game &context)
{			
	//creates textures for HUD
	context.getTexLib()->createTextTexture("narrativeCount", "Narrative Found: Changeme", 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));	
}

/*
* @param - game - the context of the game.
*/
void playState::enter(game &context)
{			
		
}

/*
* 
* @param - SDL_Window - window to draw to.
* @param - game - game context.
*/
void playState::draw(SDL_Window * window, game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
		
	rt3d::lightStruct lightEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{3.0f, 3.0f, 3.0f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular		
		std::acos(90.0f),         // spotCosCutOff
		std::acos(100.0f)         // spotExponent
	};
	
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);	
	glm::mat4 modelview(1.0); // set base position for scene	
	glm::mat3 normalMatrix(1.0); //used for normal transformation

	//Set camera
	mvStack.push(modelview);	
	context.getGameCamera()->update(context.getGamePlayer()->getPos());
	mvStack.top() = glm::lookAt(context.getGameCamera()->getEye(), context.getGameCamera()->getAt(), context.getGameCamera()->getUp());
	
	//Draws skybox
	context.getSkyBox()->draw(mvStack.top(), projection);

	glDepthMask(GL_TRUE); // ensure depth test back on after skybox
			
	glUseProgram(shaderProgram);

	//sets light
	rt3d::setLight(shaderProgram, lightEnvironment);

	//set light position, coneDirection and attenuation
	lightPos = glm::vec4(context.getGameCamera()->getEye().x, context.getGameCamera()->getEye().y, context.getGameCamera()->getEye().z, 1.0f);
	glm::vec4 tmpLight = mvStack.top() * lightPos;
	rt3d::setUniformVector4fv(shaderProgram, "lightPosition", glm::value_ptr(tmpLight));
	coneDirection = glm::vec4(context.getGameCamera()->getAt().x, context.getGameCamera()->getAt().y, context.getGameCamera()->getAt().z, 1.0f);
	glm::vec4 tmpConeDirection = mvStack.top() * coneDirection;
	rt3d::setUniformVector4fv(shaderProgram, "coneDirection", glm::value_ptr(tmpConeDirection));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
		
	//set projection
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));	
	
	//draw objects
	context.getGameObjects()->draw(mvStack, context.getGameCamera(), shaderProgram);

	context.getGamePlayer()->draw(mvStack.top(), shaderProgram);
			
	mvStack.pop(); //last pop
	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - game - the context of the game.
*/
void playState::update(game &context)
{		
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	
	//player controls
	//moves player, if collision occurs, moves player in reverse direction
	//from what key was pressed.
	if ( keys[SDL_SCANCODE_W] )
	{
		context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
		context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
		bool collision = playerObjectCollision(context);
		if(collision == true)		
			context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
	}
	if ( keys[SDL_SCANCODE_S] )
	{
		context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
		context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
		bool collision = playerObjectCollision(context);
		if(collision == true)		
			context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
	}
	if ( keys[SDL_SCANCODE_A] )
	{
		context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
		context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
		bool collision = playerObjectCollision(context);
		if(collision == true)		
			context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
	}
	if ( keys[SDL_SCANCODE_D] ) 
	{
		context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
		context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
		bool collision = playerObjectCollision(context);
		if(collision == true)		
			context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
	}
	
	//camera controls
	//rotates camera, if collision occurs, rotates reverse direction
	//from what key was pressed.
	if ( keys[SDL_SCANCODE_Q] || keys[SDL_SCANCODE_LEFT] )
	{
		context.getGameCamera()->rotateLeft();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateRight();	
	}
	if ( keys[SDL_SCANCODE_E] || keys[SDL_SCANCODE_RIGHT] ) 
	{
		context.getGameCamera()->rotateRight();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateLeft();				
	}
	if ( keys[SDL_SCANCODE_R] || keys[SDL_SCANCODE_UP] )
	{	
		context.getGameCamera()->lookUp();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookDown();			
				
	}
	if ( keys[SDL_SCANCODE_F] || keys[SDL_SCANCODE_DOWN] ) 
	{
		context.getGameCamera()->lookDown();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookUp();				
	}
	
	//Check if player reached end of level
	playerEndCollision(context);	
}

/*
* Returns if player has collided with an object - current ignores billboards and ground.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::playerObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "ground" && context.getGameObjects()->getObject(i)->getType() != "archway")
			if(context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)
				return true;		
	return false;		
}

/*
* Returns if camera has collided with an object - current ignores billboards.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::cameraObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "archway")
			if(context.getGameCamera()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)			
				return true;		
	return false;
}

/*
* Sets new state if player has reached exit for level.
* @param - game - the game context.
*/
void playState::playerEndCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 145.0f && context.getGamePlayer()->getPos().x < 160.0f
		&& context.getGamePlayer()->getPos().z > 60.0f && context.getGamePlayer()->getPos().z < 80.0f)
			context.setState(context.getEndState());
	
}

/*
* @param - game - the context of the game.
*/
void playState::exit(game &context)
{		
}