//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "game.h"
#include "introState.h"

/*
* Constructor.
*/
introState::introState(void)
{	
}

/*
* Variable Constructor.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for state.
*/
introState::introState(GLuint mesh, GLuint count, GLuint shader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;
	scaleText = 0.01f;
}

/*
* Deconstructor.
*/
introState::~introState(void)
{
}

/*
* Create text textures. Updates vector to hold texture names
* created. The vector is used to reduce draw function.
* @param - game - the context of the game.
*/
void introState::init(game &context)
{			
	context.getTexLib()->createTextTexture("title-GameName", "Dante", 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(0, 0, 255));
	context.getTexLib()->createTextTexture("title", "Prototype", 15, glm::vec3(0.0f, 0.3f, 0.0f), glm::vec3(0, 0, 255));
	context.getTexLib()->createTextTexture("title-Level", "Level 2", 15, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0, 0, 255));
		
	textureNames.push_back("title-GameName");	
	textureNames.push_back("title");	
	textureNames.push_back("title-Level");	
}

/*
* @param - game - the context of the game.
*/
void introState::enter(game &context)
{		
}

/*
* Displays the intro.
* @param - SDL_Window - window to draw to.
* @param - game - game context.
*/
void introState::draw(SDL_Window * window, game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(shaderProgram);

	for(int i = 0; i < textureNames.size(); i++)
	{
		glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj(textureNames[i], "text")->getTexture());	
		mvStack.push(glm::mat4(1.0f));	
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(context.getTexLib()->getTextureObj(textureNames[i], "text")->getPosition()));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(context.getTexLib()->getTextureObj(textureNames[i], "text")->getWidth()*scaleText,
			context.getTexLib()->getTextureObj(textureNames[i], "text")->getHeight()*scaleText, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
	}	

	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* Press any key to proceed.
* @param - game - the context of the game.
*/
void introState::update(game &context)
{		
	SDL_Event sdlEvent;	
	SDL_PollEvent(&sdlEvent);
	if (sdlEvent.type == SDL_KEYDOWN)
		context.setState(context.getPlayState());
}

/*
* Deletes textures created for intro and clears vector with names
* as intro is only run at game load.
* @param - game - the context of the game.
*/
void introState::exit(game &context)
{		
	for(int i = 0; i < textureNames.size(); i++)
		context.getTexLib()->deleteTexture(textureNames[i], "text");
	textureNames.clear();
}