//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "textureObject.h"

/*
* Default Constructor.
*/
textureObject::textureObject(void)
{
}

/*
* Deconstructor.
*/
textureObject::~textureObject(void)
{	
}

/*
* Constructor used for image textures, sets unused values to zero.
* @param - std::string - name of texture.
* @param - std::string - type of texture, e.g. text or object.
* @param - char* - name of file.
* @param - GLuint - the texture.
*/
textureObject::textureObject(std::string mName, std::string mType, char* mText, GLuint mTexture)
{
	name = mName;
	type = mType;
	text = mText;
	width = 0;
	height = 0;
	textColour = glm::vec3(0.0f, 0.0f, 0.0f);
	texture = mTexture;
}

/*
* Constructor used for text textures.
* @param - std::string - name of texture.
* @param - std::string - type of texture, e.g. text or object.
* @param - char* - text to be displayed.
* @param - GLuint - width of texture.
* @param - GLuint - height of texture.
* @param - glm::vec3 - position of text.
* @param - glm::vec3 - colour of text.
* @param - GLuint - the texture.
*/
textureObject::textureObject(std::string mName, std::string mType, char* mText, GLuint mWidth, GLuint mHeight, glm::vec3 pos, glm::vec3 mTextColour, GLuint mTexture)
{
	name = mName;
	type = mType;
	text = mText;
	width = mWidth;
	height = mHeight;
	textPosition = pos;
	textColour = mTextColour;
	texture = mTexture;
}