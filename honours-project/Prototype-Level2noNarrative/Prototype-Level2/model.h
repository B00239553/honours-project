//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "mesh.h"
#include <SDL_image.h>

#ifndef MODEL_H
#define MODEL_H

/*
* Model class - loads in model using assimp.
* Based on code from www.learnopengl.com/#!Model-Loading/Model.
*/
class model
{
private:
	std::vector<mesh> meshes;
	std::vector<Texture> textures_loaded;
	std::string directory;	
	void loadModel(std::string path);
	void processNode(aiNode* node, const aiScene* scene);
	mesh processMesh(aiMesh* meshIn, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
	GLuint loadPNG(const char* fileName, std::string path);
public:
	model(void);
	~model(void);
	model(std::string path);
	void init(void);
	std::vector<glm::vec3> getVerts(void); 	
	void draw(GLuint shader);
	void playerDraw(GLuint shader, glm::mat4 mv, glm::mat4 leftArm, glm::mat4 rightArm);
};

#endif