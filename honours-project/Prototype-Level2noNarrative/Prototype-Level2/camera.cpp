//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "camera.h"

/*
* Constructor.
*/
camera::camera(void)
{
}

/*
* Variable Constructor.
* @param - glm::vec3 - camera eye vector.
* @param - glm::vec3 - camera at vector.
* @param - glm::vec3 - camera up vector.
*/
camera::camera(glm::vec3 mEye, glm::vec3 mAt, glm::vec3 mUp)
{
	eye = mEye;
	at = mAt;
	up = mUp;
	rotation = 90.0f;
	orientation = 0.0f;
}

/*
* Deconstructor.
*/
camera::~camera(void)
{
	delete aabb;
}

/*
* Sets aabb for camera. Creates verts based on camera eye.
*/
void camera::init(void)
{
	std::vector<GLfloat> cameraVerts;
	GLfloat vert;
	vert = eye.x + 0.2f;
	cameraVerts.push_back(vert);
	vert = eye.y + 0.2f;
	cameraVerts.push_back(vert);
	vert = eye.z + 0.2f;
	cameraVerts.push_back(vert);
	vert = eye.x - 0.2f;
	cameraVerts.push_back(vert);
	vert = eye.y - 0.2f;
	cameraVerts.push_back(vert);
	vert = eye.z - 0.2f;
	cameraVerts.push_back(vert);
	aabb = new AABB("camera");
	aabb->setAABB(cameraVerts, cameraVerts.size());
}

/*
* Required to move the camera forwards and backwards.
* @param - glm::vec3 - the position of the camera.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the angle of orientation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 camera::moveForward(glm::vec3 pos, GLfloat angle, GLfloat orientation, GLfloat d) 
{
	return glm::vec3(pos.x + d*std::sin(angle*DEG_TO_RAD), pos.y + d*std::sin(orientation*DEG_TO_RAD), pos.z - d*std::cos(angle*DEG_TO_RAD));
}

/*
* Required to move the camera left and right.
* @param - glm::vec3 - the position of the camera.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 camera::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) 
{
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + d*std::sin(angle*DEG_TO_RAD));
}

/*
* Moves camera forward.
*/
void camera::moveForward(void)
{
	eye = moveForward(eye, rotation, 0.0f, 0.1f);	
}

/*
* Moves camera backwards.
*/
void camera::moveBack(void)
{
	eye = moveForward(eye, rotation, 0.0f, -0.1f);
}

/*
* Moves camera left.
*/
void camera::moveLeft(void)
{
	eye = moveRight(eye, rotation, -0.1f);
}

/*
* Moves camera right.
*/
void camera::moveRight(void)
{
	eye = moveRight(eye, rotation, 0.1f);
}

/*
* Rotates camera left.
*/
void camera::rotateLeft(void)
{
	rotation -= 1.0f;
}

/*
* Rotates camera right.
*/
void camera::rotateRight(void)
{
	rotation += 1.0f;
}

/*
* Makes camera look up, has a limit.
*/
void camera::lookUp(void)
{
	if(orientation <= 90.0f)
		orientation += 1.0f;	
}

/*
* Makes camera look down, has a limit.
*/
void camera::lookDown(void)
{
	if(orientation >= -90.0f)
		orientation -= 1.0f;	
}

/*
* Updates at and eye vectors, used for draw calls.
* Updates AABB for camera based on eye position.
* @param - glm::vec3 - new position for camera to look at and base eyes position on.
*/
void camera::update(glm::vec3 pos)
{	
	at = glm::vec3(pos.x, pos.y, pos.z);
	eye = moveForward(glm::vec3(pos.x, pos.y, pos.z), rotation, orientation, -8.0f);
	eye.y += 10.0f;
	at.y += 2.0f;
	aabb->updateAABB(eye);
}