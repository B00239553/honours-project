//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "textureLibrary.h"

/*
* Constructor.
*/
textureLibrary::textureLibrary(void)
{
}

/*
* Deconstructor.
*/
textureLibrary::~textureLibrary(void)
{
	deleteAllTextures();
}

/*
* Will not create texture if name already held.
* Used for creating image textures, calls create texture function inputing empty values for
* inputs that are not required.
* @param - std::string - name of texture.
* @param - char* - name of texture.
*/
void textureLibrary::createImageTexture(std::string name, char* fileName)
{
	bool duplicateName = false;
	for(int i = 0; i < objectTextures.size(); i++)
	{
		if(name == objectTextures[i]->getName())
		{
			duplicateName = true;			
			break;
		}
	}
	if(duplicateName == false)
		createTexture(name, "image", fileName, 0, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	else
		std::cout << "Image texture trying to be created with same name" << std::endl;
}

/*
* Will not create texture if name already held.
* Creates text texture from details taken in.
* @param - std::string - name of texture.
* @param - char* - text to be displayed.
* @param - int - size of text.
* @param - glm::vec3 - position of text.
* @param - glm::vec3 - colour of text.
*/
void textureLibrary::createTextTexture(std::string name, char* text, int size, glm::vec3 pos, glm::vec3 colour)
{
	bool duplicateName = false;
	for(int i = 0; i < textTextures.size(); i++)
	{
		if(name == textTextures[i]->getName())
		{
			duplicateName = true;			
			break;
		}
	}
	if(duplicateName == false)
		createTexture(name, "text", text, size, pos, colour);
	else
		std::cout << "Text texture trying to be created with same name" << std::endl;
}

/*
* Creates texture based on the type.
* @param - std::string - name of texture.
* @param - std::string - type of texture.
* @param - char* - for image is filename, for text is the text to be displayed.
* @param - int - size of text, only used for text textures.
* @param - glm::vec3 - colour of text, only used for text textures.
*/
void textureLibrary::createTexture(std::string name, std::string type, char* text, int size, glm::vec3 pos, glm::vec3 colour)
{
	if(type == "image")//image texture, used for objects
	{
		GLuint tmpTexture;
		loadPNG(tmpTexture, text);
		textureObject *tmpObject;
		tmpObject = new textureObject(name, type, text, tmpTexture);
		objectTextures.push_back(tmpObject);	
		tmpObject = NULL;
	}
	else if(type == "text")//text textures, created based on details from input parameters
	{
		GLuint width, height, tmpTexture;
		textToTexture(tmpTexture, width, height, text, size, colour);
		textureObject *tmpObject;
		tmpObject = new textureObject(name, type, text, width, height, pos, colour, tmpTexture);
		textTextures.push_back(tmpObject);		
		tmpObject = NULL;
	}
	else//error checking
	{
		std::cout << "Texture type is invalid at texture name: " << name << std::endl;	
		std::cout << "Texture not created." << std::endl;
	}
}

/*
* Creates text taking in to a textire. Uses references to update texture, its width and height.
* @param - GLuint - the texture ID for texture that will be created.
* @param - GLuint - the width for texture that will be created.
* @param - GLuint - the height for texture that will be created.
* @param - const char* - the string to be put on the texture.
* @param - int - the size of the text.
* @param - glm::vec3 - the colour of the text.
*/
void textureLibrary::textToTexture(GLuint &tex, GLuint &w, GLuint &h, const char* str, int s, glm::vec3 col)
{
	TTF_Font *textFont;
	
	if (TTF_Init() == -1)//error checking
		std::cout << "TTF failed to initialise." << std::endl;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", s);
	if (textFont == NULL)
		std::cout << "Failed to open font." << std::endl;


	SDL_Color colour = { col.x, col.y, col.z };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)//error checking		
		std::cout << "String surface not created." << std::endl;

	//set width and height of texture object
	w = stringImage->w;
	h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;
	
	GLuint format, internalFormat;
	if (colours == 4)
	{   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	}
	else 
	{   // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
	
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);	
}

/*
* Loads bitmap from file name taken in and creates texture using image.
* @param - GLuint - the texture ID for texture that will be created.
* @param - char* - file name of bitmap.
*/
void textureLibrary::loadBitmap(GLuint &tex, char* fileName)
{	
	glGenTextures(1, &tex); // generate texture

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fileName);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer	
}

/*
* Loads png from file name taken in and creates texture using image.
* @param - GLuint - the texture ID for texture that will be created.
* @param - char* - file name of bitmap.
*/
void textureLibrary::loadPNG(GLuint &tex, char* fileName)
{
	glGenTextures(1, &tex); // generate texture

	// load file - using core SDL library 	
	SDL_Surface* loadedSurface = IMG_Load(fileName);
	if ( !loadedSurface )
    {
		printf ( "IMG_Load: %s\n", IMG_GetError () );      
    }
			
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = loadedSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,loadedSurface->w, loadedSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, loadedSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(loadedSurface); // texture loaded, free the temporary buffer
}

/*
* Gets the texture object from name taken in.
* @param - std::string - name of texture object.
* @param - std::string - the type of texture object, used for searching correct vector.
*/
textureObject* textureLibrary::getTextureObj(std::string name, std::string type)
{
	bool found = false;
	int i = 0;
	if(type == "image")
	{
		while(found == false && i < objectTextures.size())
		{
			if(name != objectTextures[i]->getName())
				i++;
			else			
				found = true;				
		}
		return objectTextures[i];
	}
	else if(type == "text")
	{
		while(found == false && i < textTextures.size())
		{
			if(name != textTextures[i]->getName())
				i++;
			else			
				found = true;				
		}
		return textTextures[i];
	}
	else
	{
		std::cout << "Trying to get invalid texture type, returned NULL texture." << std::endl;
		return NULL;
	}
}

/*
* Deletes the texture object from name taken in.
* @param - std::string - name of texture object.
* @param - std::string - the type of texture object, used for searching correct vector.
*/
void textureLibrary::deleteTexture(std::string name, std::string type)
{
	bool found = false;
	int i = 0;
	if(type == "image")
	{
		while(found == false && i < objectTextures.size())
		{
			if(name != objectTextures[i]->getName())
				i++;
			else			
				found = true;				
		}
		delete objectTextures[i];
		objectTextures.erase(objectTextures.begin() + i);
	}
	else if(type == "text")
	{
		while(found == false && i < textTextures.size())
		{
			if(name != textTextures[i]->getName())
				i++;
			else			
				found = true;				
		}
		delete textTextures[i];
		textTextures.erase(textTextures.begin() + i);
	}
	else	
		std::cout << "Trying to delete invalid texture type" << std::endl;	
}

/*
* Deletes all texture objects and clears vectors.
*/
void textureLibrary::deleteAllTextures(void)
{
	for(int i = 0; i < objectTextures.size(); i++)
	{
		delete objectTextures[i];
	}
	objectTextures.clear();

	for(int i = 0; i < textTextures.size(); i++)
	{
		delete textTextures[i];
	}
	textTextures.clear();
}