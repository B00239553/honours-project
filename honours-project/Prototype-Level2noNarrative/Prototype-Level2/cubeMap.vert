//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

// cubeMap.vert
#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout(location = 0) in vec3 in_Position;

smooth out vec3 cubeTexCoord;

void main(void) {

	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
    gl_Position = projection * vertexPosition;

	cubeTexCoord = normalize(in_Position);	
}