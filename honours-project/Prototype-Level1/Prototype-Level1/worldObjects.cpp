//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "worldObjects.h"


/*
* Constructor.
*/
worldObjects::worldObjects(void)
{
	AABBset = false;
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - STD::vector<GLfloat> - verts of cube, used for AABB creation.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of cube indices.
* @param - STD::vector<GLfloat> - verts of billboard, used for AABB creation.
* @param - GLuint - mesh of billboard.
* @param - GLuint - count of billboard indices.
*/
worldObjects::worldObjects(std::vector<GLfloat> cubeV, GLuint cubeMesh, GLuint cubeCount, std::vector<GLfloat> bbV, GLuint bbMesh, GLuint bbCount)
{	
	cubeVerts = cubeV;
	cubeObject = cubeMesh;
	cubeIndex = cubeCount;
	bbVerts = bbV;
	bbObject = bbMesh;
	bbIndex = bbCount;
	AABBset = false;
}

/*
* Deconstructor.
*/
worldObjects::~worldObjects(void)
{	
	deleteAllObjects();
}

/*
* Will create textures to be used for objects and create objects for world.
* @param - &textureLibrary - the texture library used to store textures.
*/
void worldObjects::init(textureLibrary &texLib)
{	
	texLib.createImageTexture("bush", "bush.png");
	texLib.createImageTexture("short stump tree", "short stump tree.png");
	texLib.createImageTexture("split stump tree", "split stump tree.png");
	texLib.createImageTexture("old stump tree", "old stump tree.png");
	texLib.createImageTexture("tall tree", "tall tree.png");
	texLib.createImageTexture("grass", "grass.png");
	texLib.createImageTexture("hedge", "hedge.png");
	
	object *tmpObject = new object("grass", "ground", texLib.getTextureObj("grass", "image")->getTexture(), glm::vec3(0.0f, -3.0f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(120.0f, 1.0f, 120.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("hedge1", "hedge", texLib.getTextureObj("hedge", "image")->getTexture(), glm::vec3(0.0f, 1.5f, -60.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(120.0f, 10.0f, 1.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("hedge2", "hedge", texLib.getTextureObj("hedge", "image")->getTexture(), glm::vec3(7.5f, 1.5f, 60.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(105.0f, 10.0f, 1.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("hedge3", "hedge", texLib.getTextureObj("hedge", "image")->getTexture(), glm::vec3(-55.0f, 1.5f, 60.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(10.0f, 10.0f, 1.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("hedge4", "hedge", texLib.getTextureObj("hedge", "image")->getTexture(), glm::vec3(60.0f, 1.5f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(1.0f, 10.0f, 120.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("hedge5", "hedge", texLib.getTextureObj("hedge", "image")->getTexture(), glm::vec3(-60.0f, 1.5f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(1.0f, 10.0f, 120.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);


	tmpObject = new object("bush1", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-40.0f, 1.5f, 50.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush2", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-45.0f, 1.5f, 50.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush3", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(50.0f, 1.5f, 52.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush4", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(52.5f, 1.5f, 10.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush5", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(48.0f, 1.5f, -3.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush6", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(26.0f, 1.5f, -55.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush7", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(40.0f, 1.5f, -55.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush8", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(53.0f, 1.5f, -39.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush9", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-5.0f, 1.5f, -21.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush10", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(5.0f, 1.5f, -21.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush11", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-56.0f, 1.5f, -49.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush12", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-29.0f, 1.5f, -49.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("bush13", "billboard", texLib.getTextureObj("bush", "image")->getTexture(), glm::vec3(-39.0f, 1.5f, 15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("shorttree1", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-45.0f, 11.3f, 48.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree2", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-52.0f, 11.3f, 39.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree3", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-38.0f, 11.3f, 43.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree4", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(5.0f, 11.3f, 25.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree5", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(3.0f, 11.3f, 35.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree6", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-20.0f, 11.3f, 25.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree7", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(42.0f, 11.3f, 28.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree8", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(41.0f, 11.3f, 45.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree9", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(32.0f, 11.3f, 15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree10", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(50.0f, 11.3f, -15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree11", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(28.0f, 11.3f, -38.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree12", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(52.0f, 11.3f, -33.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree13", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-13.0f, 11.3f, -53.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree14", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(11.0f, 11.3f, -24.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree15", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-44.0f, 11.3f, -45.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree16", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-42.0f, 11.3f, -10.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("shorttree17", "billboard", texLib.getTextureObj("short stump tree", "image")->getTexture(), glm::vec3(-30.0f, 11.3f, 7.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 35.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("splittree1", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(-30.0f, 4.0f, 46.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree2", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(17.0f, 4.0f, 45.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree3", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(30.0f, 4.0f, 40.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree4", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(25.0f, 4.0f, -15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree5", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(37.0f, 4.0f, -54.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree6", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(41.0f, 4.0f, -23.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree7", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(-13.0f, 4.0f, -23.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree8", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(11.0f, 4.0f, -46.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree9", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(-33.0f, 4.0f, -28.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("splittree10", "billboard", texLib.getTextureObj("split stump tree", "image")->getTexture(), glm::vec3(-32.0f, 4.0f, -15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("oldtree1", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-31.0f, 4.0f, 31.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree2", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-10.0f, 4.0f, 42.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree3", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(33.0f, 4.0f, 31.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree4", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(43.0f, 4.0f, 15.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree5", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(25.0f, 4.0f, -4.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree6", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(30.0f, 4.0f, -45.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree7", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(47.0f, 4.0f, -50.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree8", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(53.0f, 4.0f, -22.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree9", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-13.0f, 4.0f, -46.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree10", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(10.0f, 4.0f, -35.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree11", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-23.0f, 4.0f, -21.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree12", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-38.0f, 4.0f, -38.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree13", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-43.0f, 4.0f, 1.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldtree14", "billboard", texLib.getTextureObj("old stump tree", "image")->getTexture(), glm::vec3(-25.0f, 4.0f, 18.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("talltree1", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-40.0f, 4.0f, 25.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree2", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-10.0f, 4.0f, 23.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree3", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(22.0f, 4.0f, 26.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree4", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(32.0f, 4.0f, -9.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree5", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(24.0f, 4.0f, -22.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree6", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(51.0f, 4.0f, -43.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree7", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-13.0f, 4.0f, -35.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree8", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(10.0f, 4.0f, -55.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree9", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-28.0f, 4.0f, -36.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree10", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-55.0f, 4.0f, -25.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree11", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-55.0f, 4.0f, -12.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("talltree12", "billboard", texLib.getTextureObj("tall tree", "image")->getTexture(), glm::vec3(-24.0f, 4.0f, -7.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 1.0f), bbObject, bbIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = NULL;
}

/*
* Deletes all objects held.
*/
void worldObjects::deleteAllObjects(void)
{
	for(int i = 0; i < sceneObjects.size(); i++)
		delete sceneObjects[i];
	sceneObjects.clear();	
}

/*
* Draws all world objects.
* First call will set AABBs for all objects held by calling their set function,
* this is done so AABBs are set after scaling and rotation operations.
* @param - std::stack<glm::mat4> - the current modelview matrix.
* @param - camera - current game camera.
* @param - GLuint - current shader to be used.
*/
void worldObjects::draw(std::stack<glm::mat4> mvStack, camera *gameCam, GLuint shader)	
{
	rt3d::materialStruct materialEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse 
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		2.0f  // shininess
	};
		
	rt3d::setMaterial(shader, materialEnvironment);
	glm::mat3 normalMatrix(1.0);

	for(int i = 0; i < sceneObjects.size(); i++)
	{		
		glDisable(GL_CULL_FACE);		
		if(sceneObjects[i]->getType() == "billboard")
		{//Updates object rotation vector and rotation angle so is always pointed towards camera eye.
			glm::vec3 objToCamProj = glm::vec3(gameCam->getEye().x - sceneObjects[i]->getTranslate().x, 0.0f,
				gameCam->getEye().z - sceneObjects[i]->getTranslate().z);
			objToCamProj = glm::normalize(objToCamProj);
			glm::vec3 modelLookAt = glm::vec3(0.0f, 0.0f, 1.0f);
			float dotModelEye = glm::dot(modelLookAt, objToCamProj);
			float modelMag = sqrt(modelLookAt.x*modelLookAt.x + modelLookAt.y*modelLookAt.y + modelLookAt.z*modelLookAt.z);
			float objToEyeMag = sqrt(objToCamProj.x*objToCamProj.x + objToCamProj.y*objToCamProj.y + objToCamProj.z*objToCamProj.z);
			sceneObjects[i]->setRotate(glm::cross(modelLookAt, objToCamProj));
			float angleCosine = std::acos(dotModelEye/(modelMag*objToEyeMag)) * RAD_TO_DEG;
			sceneObjects[i]->setRotation(angleCosine);					
		}
		glUseProgram(shader);
		mvStack.push(mvStack.top());		
		glBindTexture(GL_TEXTURE_2D, sceneObjects[i]->getTex());		
		mvStack.top() = glm::translate(mvStack.top(), sceneObjects[i]->getTranslate());	
		if(sceneObjects[i]->getRotation() != 0.0f)
			if(sceneObjects[i]->getRotate() != glm::vec3(0.0f, 0.0f, 0.0f))
				mvStack.top() = glm::rotate(mvStack.top(), sceneObjects[i]->getRotation(), sceneObjects[i]->getRotate());		
		mvStack.top() = glm::scale(mvStack.top(), sceneObjects[i]->getScale());			
		normalMatrix = glm::transpose(glm::inverse(glm::mat3(mvStack.top())));	
		rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));		
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(sceneObjects[i]->getMeshObject(),sceneObjects[i]->getMeshIndexCount(),GL_TRIANGLES);
		
		//Sets AABB for world objects, only set once as bool will be true after first call.
		if(AABBset == false)
		{	
			glm::mat4 colMat(1.0);
			colMat = glm::translate(colMat, sceneObjects[i]->getTranslate());	
			if(sceneObjects[i]->getRotate() != glm::vec3(0.0f, 0.0f, 0.0f))
				colMat = glm::rotate(colMat, sceneObjects[i]->getRotation(), sceneObjects[i]->getRotate());		
			colMat = glm::scale(colMat, sceneObjects[i]->getScale());	
			std::vector<GLfloat> tmpVerts;
			if(sceneObjects[i]->getType() == "ground" || sceneObjects[i]->getType() == "hedge")
				tmpVerts = cubeVerts;
			else
				tmpVerts = bbVerts;
			for(int i = 0; i < tmpVerts.size(); i=i+3)
			{
				glm::vec4 tempVec = colMat * glm::vec4(tmpVerts[i], tmpVerts[i+1], tmpVerts[i+2], 1.0);				
				tmpVerts[i] = tempVec.x;
				tmpVerts[i+1] = tempVec.y;
				tmpVerts[i+2] = tempVec.z;	
			}
			sceneObjects[i]->setAABB(tmpVerts);				
		}		
		mvStack.pop();
	}	
	if(AABBset == false)
		AABBset = true;	
	glEnable(GL_CULL_FACE);
}