//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef OBJECT_LOADER
#define OBJECT_LOADER

#include "rt3d.h"
#include <vector>
#include <sstream>

/*
* Library used for loading triangulated OBJ models. 
* Is very basic loader and does not support, groups, multiple meshes and will not generate any data missing.
* Only supports straight forward OBJ models. Based on rt3dObjLoader library created by Daniel Livingstone.
*/
namespace objectLoader
{
	void loadObject(const char* fileName, std::vector<GLfloat> &verts, std::vector<GLfloat> &norms, 
		std::vector<GLfloat> &texCoords, std::vector<GLuint> &indices);
};

#endif