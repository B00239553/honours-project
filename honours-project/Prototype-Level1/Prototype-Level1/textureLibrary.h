//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef TEXTURE_LIBRARY
#define TEXTURE_LIBRARY

#include "textureObject.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <vector>
#include <iostream>

/*
* Texture Library - Will create textures(image or text), 
* will store them and give access to retrieve them.
*/
class textureLibrary
{
private:	
	std::vector<textureObject*> objectTextures;
	std::vector<textureObject*> textTextures;
	void textToTexture(GLuint &tex, GLuint &w, GLuint &h, const char* str, int size, glm::vec3 col);
	void loadBitmap(GLuint &tex, char* fileName);	
	void loadPNG(GLuint &tex, char* fileName);
	void createTexture(std::string name, std::string type, char* text, int s, glm::vec3 pos, glm::vec3 colour);
public:
	textureLibrary(void);
	~textureLibrary(void);
	void init(void);
	void createImageTexture(std::string name, char* fileName);
	void createTextTexture(std::string name, char* text, int s, glm::vec3 pos, glm::vec3 colour);	
	textureObject* getTextureObj(std::string name, std::string type);
	void deleteTexture(std::string name, std::string type);
	void deleteAllTextures(void);

};

#endif