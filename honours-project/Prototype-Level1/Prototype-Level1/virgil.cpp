//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "virgil.h"

/*
* Constructor.
*/
virgil::virgil(void)
{
}

/*
* Deconstructor.
*/
virgil::~virgil(void)
{
	delete virgilModel;
}

/*
* Takes in file path for model to be loaded.
*/
virgil::virgil(std::string mPath)
{
	modelCentre = glm::vec3(0.0f, 0.0f, 0.0f);
	modelPath = mPath;	
	velocity = 0.1f;
	position = glm::vec3(-47.5f, -2.5f, 55.0);
	rotate = glm::vec3(0.0f, 1.0f, 0.0f);
	rotation = 0.0f;
	modelRotation = 180.0f;
	aabbSet = false;
	aabb = new AABB();
}

/*
* Loads and creates model/mesh.
*/
void virgil::init(void)
{
	virgilModel = new model(modelPath);
	virgilModel->init();
}

/*
* Required to move virgil forwards and backwards.
* @param - glm::vec3 - the position of virgil.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 virgil::movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance) 
{
	return glm::vec3(pos.x + distance*std::sin(angle*DEG_TO_RAD), pos.y, pos.z - distance*std::cos(angle*DEG_TO_RAD));
}

/*
* Required to move virgil left and right.
* @param - glm::vec3 - the position of virgil.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 virgil::movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance)
{
	return glm::vec3(pos.x + distance*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + distance*std::sin(angle*DEG_TO_RAD));
}

/*
* Moves virgil forward.
*/
void virgil::moveForward(void)
{		
	position = movementForward(position, rotation, velocity);	
}

/*
* Moves virgil backwards.
*/
void virgil::moveBack(void)
{	
	modelRotation = 0.0f;
	position = movementForward(position, rotation, -velocity);	
}

/*
* Moves virgil right.
*/
void virgil::moveRight(void)
{	
	modelRotation = 90.0f;
	position = movementRight(position, rotation, velocity);	
}

/*
* Moves virgil left.
*/
void virgil::moveLeft(void)
{	
	modelRotation = 270.0f;
	position = movementRight(position, rotation, -velocity);	
}

/*
* Moves virgil until colliding with dante.
* @param - AABB - dante's aabb.
*/
void virgil::seek(AABB dante)
{
	if(aabb->AABBcollision(dante) == false)
	{
		glm::vec3 virgilToDante = glm::vec3(dante.getCentre().x - position.x, 0.0f, dante.getCentre().z - position.z);	//new look at vector
		glm::vec3 modelLookAt = glm::vec3(0.0f, 0.0f, 1.0f); //model look at vector
		//calculates polar coordinates to find angle Virgil should look at
		float dotModelPos = glm::dot(modelLookAt, virgilToDante);
		float modelMag = sqrt(modelLookAt.x*modelLookAt.x + modelLookAt.y*modelLookAt.y + modelLookAt.z*modelLookAt.z);
		float NPCToPosMag = sqrt(virgilToDante.x*virgilToDante.x + virgilToDante.y*virgilToDante.y + virgilToDante.z*virgilToDante.z);
		modelRotation = std::acos(dotModelPos/(modelMag*NPCToPosMag)) * RAD_TO_DEG;	
		rotate = glm::cross(modelLookAt, virgilToDante);			
		if(rotate.y < 0) //change player rotation based on cross to know whether angle is positive or negative
			rotation = modelRotation + 180;
		else
			rotation = -modelRotation + 180;
		moveForward();	
		aabb->updateAABB(position);
	}	
}

/*
* Draws virgil.
* @param - glm::mat4 - modelview to be used.
* @param - GLuint - shader to be used.
*/
void virgil::draw(glm::mat4 modelview, GLuint shader)
{
	glUseProgram(shader);
	glCullFace(GL_BACK);
	modelview = glm::translate(modelview, position);
	modelview = glm::rotate(modelview, modelRotation, rotate);
	modelview = glm::scale(modelview, glm::vec3(1.8f, 1.8f, 1.8f));		
	if(aabbSet == false)
	{	
		glm::mat4 colMat(1.0f);
		colMat = glm::translate(colMat, position);
		colMat = glm::rotate(colMat, modelRotation, glm::vec3(0.0f, 1.0f, 0.0f));
		colMat = glm::scale(colMat, glm::vec3(1.8f, 1.8f, 1.8f));
		std::vector<glm::vec3> tmpVerts = virgilModel->getVerts();			
		for(int i = 0; i < tmpVerts.size(); i++)
		{
			glm::vec4 tempVec = colMat * glm::vec4(tmpVerts[i].x, tmpVerts[i].y, tmpVerts[i].z, 1.0);
			tmpVerts[i].x = tempVec.x;
			tmpVerts[i].y = tempVec.y;
			tmpVerts[i].z = tempVec.z;	
		}
		aabb->setAABBvec3(tmpVerts);	
		aabbSet = true;
	}	
	glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelview)));	
	rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	virgilModel->draw(shader);
	glCullFace(GL_FRONT);
}