//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

//text.vert.
//Displays text texture as projection.
#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout(location = 0) in vec3 in_Position;
layout(location = 2) in vec2 in_TexCoord;

out vec2 ex_TexCoord;

void main(void)
{
	vec4 vertexPosition = modelview * vec4(in_Position, 1.0);
	gl_Position = projection * vertexPosition;
	
	ex_TexCoord = in_TexCoord;	
}
