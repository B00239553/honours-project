//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "objectLoader.h"

#define FORMAT_UNKNOWN 0
#define FORMAT_V 1
#define FORMAT_VT 2
#define FORMAT_VTN 3
#define FORMAT_VN 4

namespace objectLoader
{
	/*
	* Position of vertice.
	*/
	struct position
	{
		GLfloat x;
		GLfloat y;
		GLfloat z;
	};

	/*
	* Type of vector.
	*/
	struct faceIndex
	{
		int v;
		int t;
		int n;
	};

	/*
	* Determines type of data.
	* @param - std::string - used for finding type of data.
	* @return - int - returns data type.
	*/
	int determineFaceFormat(std::string fString)
	{
		unsigned int delim1 = fString.find('/');
		if (delim1 == std::string::npos)
			return FORMAT_V;		
		unsigned int delim2 = fString.rfind('/');
		if (delim1 == delim2)
			return FORMAT_VT;
		if (delim2 == (delim1+1))
			return FORMAT_VN;
		return FORMAT_VTN;
	}

	/*
	* Returns vertex, texCoords and normal index.
	* @param - std::string - data read in.
	* @param - int - type of data.
	* @return - faceIndex - data structure of vertice, texCoordinates and normal.
	*/
	faceIndex getFace(std::string fString, int fFormat) {		
		int delim1 = fString.find('/');
		int delim2 = fString.rfind('/');
		
		std::string vstr = fString.substr(0, delim1);
		std::stringstream buffer;
		if ( fFormat == FORMAT_VT )
		{
			// v/t format
			std::string tstr = fString.substr(delim1+1, fString.size() );
			buffer << vstr << " " << tstr << " " << 0 << " ";
		}
		else 
		{
			// v/t/n format or v//n format
			std::string nstr = fString.substr(delim2+1, fString.size()-delim2 );
			if ( fFormat == FORMAT_VTN ) 
			{
				std::string tstr = fString.substr(delim1+1, delim2-(delim1+1) );
				buffer << vstr << " " << tstr << " " << nstr << " ";
			}
			else
				buffer << vstr << " " << 0 << " " << nstr << " ";
		}
		
		faceIndex f;
		buffer >> f.v >> f.t >> f.n;
		f.v--; f.t--, f.n--;
		buffer.clear();
		return f;
	}

	/*
	* Adds vertex, related texture coordinates and normals.
	* @param - std::string - the format of the data.
	* @param - std::map<std::string,GLuint> - map of object data.
	* @param - std::vector<position> - vertices that have been read in vector.
	* @param - std::vector<position> - texture coordinates that have been read invector.
	* @param - std::vector<position> - normals that have been read in vector.
	* @param - std::vector<GLfloat> - vertices vector to be updated.
	* @param - std::vector<GLfloat> - texture coordinates to be updated.
	* @param - std::vector<GLfloat> - normals vector to be updated.
	* @param - std::vector<GLuint> - indices to be updated.
	* @param - int - the type of data.
	* @param - int - used for indices.
	*/
	void addVertex(std::string fString1, std::map<std::string,GLuint> &indexMap, 
		std::vector<position> &inVerts, std::vector<position> &inCoords, std::vector<position> &inNorms, 
		std::vector<GLfloat> &verts, std::vector<GLfloat> &texcoords, std::vector<GLfloat> &norms, 
		std::vector<GLuint> &indices, int fFormat, int &index)
	{

		auto itr = indexMap.find(fString1);
		if (itr == indexMap.end())
		{
			faceIndex f = getFace(fString1, fFormat);
			verts.push_back(inVerts[f.v].x);
			verts.push_back(inVerts[f.v].y);
			verts.push_back(inVerts[f.v].z);
			if (fFormat < FORMAT_VN) 
			{
				texcoords.push_back(inCoords[f.t].x);
				texcoords.push_back(inCoords[f.t].y);
			}
			if (fFormat > FORMAT_VT)
			{
				norms.push_back(inNorms[f.n].x);
				norms.push_back(inNorms[f.n].y);
				norms.push_back(inNorms[f.n].z);
			}
			indexMap.insert(std::pair<std::string, GLuint>(fString1, index)); 
			indices.push_back(index++);
		}
		else 
		{
			indices.push_back( itr->second );
		}
	}

	/*
	* Loads object fron file taken in.
	* @param - const char* - name of file/
	* @param - std::vector<GLfloat> - vertice vector to be updated.
	* @param - std::vector<GLfloat> - normal vector to be updated.
	* @param - std::vector<GLfloat> - texture coordinates to be updated.
	* @param - std::vector<GLuint> - indices to be updated.
	*/
	void loadObject(const char* fileName, std::vector<GLfloat> &verts, std::vector<GLfloat> &norms, 
		std::vector<GLfloat> &texCoords, std::vector<GLuint> &indices)
	{
		GLint fileLength;
		char *fileSource = rt3d::loadFile(fileName, fileLength);

		if (fileLength == 0)
			std::cout << "No file to be read in. " << std::endl;

		std::stringstream  fileStream;
		fileStream << fileSource;

		char line[256];
		std::string lineHeader;
		std::string fString1;
		std::string fString2;
		std::string fString3;
		std::stringstream buffer;
		std::vector<position> inVerts;
		std::vector<position> inNorms;
		std::vector<position> inCoords;
		
		GLfloat x,y,z;
		GLint a,b,c;

		int i=0, iCount = 0;
		position tmp;
		std::map<std::string,GLuint> indexMap;
		int fFormat = FORMAT_UNKNOWN;

		std::cout << "started parsing obj image..." << std::endl;

		while (fileStream.good()) 
		{			
			fileStream >> lineHeader;
			switch (lineHeader[0]) 
			{
				case 'v':
					if ( lineHeader.length() > 1)
						switch (lineHeader[1]) 
					{
						case 't': 
							fileStream >> tmp.x >> tmp.y;
							inCoords.push_back(tmp);
							break;
						case 'n':
							fileStream >> tmp.x >> tmp.y >> tmp.z;
							inNorms.push_back(tmp);
							break;
						default:
							break;
					}
					else 
					{
						fileStream >> tmp.x >> tmp.y >> tmp.z;
						inVerts.push_back(tmp);
					}
					break;
				case 'f':
					fileStream >> fString1 >> fString2 >> fString3;
					if (!fFormat)
						fFormat = determineFaceFormat(fString1);
					if (fFormat > FORMAT_V)
					{
						addVertex(fString1, indexMap, inVerts, inCoords, inNorms, verts, texCoords, norms, indices, fFormat, iCount);
						addVertex(fString2, indexMap, inVerts, inCoords, inNorms, verts, texCoords, norms, indices, fFormat, iCount);
						addVertex(fString3, indexMap, inVerts, inCoords, inNorms, verts, texCoords, norms, indices, fFormat, iCount);
					}
					else 
					{
						buffer << fString1 << " " << fString2 << " " << fString3;
						buffer >> a >> b >> c;
						buffer.clear();
						indices.push_back(a-1);
						indices.push_back(b-1);
						indices.push_back(c-1);
					}
					break;
				case '#': 
					fileStream.getline(line, 256); // ignore line
					break;
				default:
					fileStream.getline(line, 256); // ignore line
				}
		}

		// copy vertex data to output vectors in case only single index was provided....
		if (fFormat == FORMAT_V) 
		{
			for (int v = 0; v < inVerts.size(); v++) 
			{
				verts.push_back(inVerts[v].x);
				verts.push_back(inVerts[v].y);
				verts.push_back(inVerts[v].z);			
			}
		}
		std::cout << "finished parsing obj image..." << std::endl;
	}
}