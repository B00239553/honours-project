//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef PLAYSTATE_H   
#define PLAYSTATE_H   

#define DEG_TO_RAD 0.017453293

/*
* Play State - Prototype is played from this state.
*/
class playState : public gameState {
private:
	bool lockControls;
	bool enterVirgil;
	bool tip1;
	bool tip2;
	bool tip3;
	std::vector<std::string> narrativeNames;
	int narrativeText;
	clock_t narrativeTimer;
	clock_t keyTimer;
	std::stack<glm::mat4> mvStack;	
	glm::vec4 lightPos;	
	glm::vec4 coneDirection;
	glm::vec3 attenuation;	
	GLuint hudShader;
	bool playerObjectCollision(game &context);
	bool cameraObjectCollision(game &context);
	void playerEndCollision(game &context);	
public:			
	playState(void);	
	playState(GLuint mesh, GLuint count, GLuint objShader, GLuint hShader);
	~playState(void);
	void draw(SDL_Window * window, game &context);	
	void init(game &context);	
	void update(game &context);	
	void enter(game &context);
	void exit(game &context);		
};

#endif 