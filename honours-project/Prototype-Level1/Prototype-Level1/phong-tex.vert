//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

//phong-tex.vert.
//Vertex shader to use with Phong reflection model fragment shader.
//Calculates passes on V, L, N vectors for use in fragment shader.
#version 330

uniform mat4 modelview;
uniform mat4 projection;
uniform mat3 normalmatrix;
uniform vec4 lightPosition;

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Normal;
out vec3 ex_V;
out vec3 ex_N;
out vec3 ex_L;

layout(location = 2) in vec2 in_TexCoord;
out vec2 ex_TexCoord;

// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void)
{
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;

	// surface normal in eye coordinates		
	ex_N = normalize(normalmatrix * in_Normal);

	// L - vector to light source from vertex	
	ex_L = lightPosition.xyz - vertexPosition.xyz;		
	
	ex_TexCoord = in_TexCoord; 
	
    gl_Position = projection * vertexPosition;
}

