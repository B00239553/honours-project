//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef SKYBOX_H
#define SKYBOX_H

#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>

/*
* Skybox Class - Will load textures, create skybox and will be drawn here.
*/
class skybox
{
private:
	glm::vec3 skyboxScale;
	GLuint skyboxShader;
	GLuint meshObject;
	GLuint meshIndexCount;
	GLuint skyboxTexture;
	void loadCubeMap(const char* fname[6], GLuint *texID);	
public:
	skybox(void);
	skybox(GLuint shader, GLuint mesh, GLuint count);
	~skybox(void);
	void init(void);
	void draw(glm::mat4 modelview, glm::mat4 projection);
};

#endif