//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "game.h"
#include "playState.h"

/*
* Constructor.
*/
playState::playState(void)
{
}

/*
* Variable Constructor.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for objects.
* @param - GLuint - shader program for HUD.
*/
playState::playState(GLuint mesh, GLuint count, GLuint objShader, GLuint hShader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = objShader;
	hudShader = hShader;	
	attenuation = glm::vec3(1.0f, 0.0f, 0.005f);
	tip1 = false;
	tip2 = false;
	tip3 = false;
}

/*
* Deconstructor.
*/
playState::~playState(void)
{
}

/*
* 
* @param - game - the context of the game.
*/
void playState::init(game &context)
{			
	//creates textures for HUD	
	context.getTexLib()->createTextTexture("healthBar", "Health:", 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
	context.getTexLib()->createImageTexture("black", "black.png");
	context.getTexLib()->createImageTexture("red", "red.png");	
	std::stringstream str;
	str << "Deaths: 0";	
	context.getTexLib()->createTextTexture("playerDeaths", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));	
	
	context.getTexLib()->createTextTexture("deaths", "Deaths:", 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
	narrativeText = -1;
	context.getTexLib()->createTextTexture("virgil1", "\"DANTE!!\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil1");
	context.getTexLib()->createTextTexture("dante1",  "Dante: \"Have pity on me, whatever you are, shadow or definite man.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante1");
	context.getTexLib()->createTextTexture("virgil2", "\"Not a man, though I was one.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2");
	context.getTexLib()->createTextTexture("virgil3", "\"I was a poet, I sang of the just Son of Anchises, the man who came from Troy.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil3");
	context.getTexLib()->createTextTexture("dante2",  "Dante: \"Are you indeed that Virgil, are you the spring which spreads abroad\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante2");
	context.getTexLib()->createTextTexture("dante3",  "Dante: \"that wide water of speech. Look at the animals which have made me\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante3");
	context.getTexLib()->createTextTexture("dante4",  "Dante: \"turn back, help me handle them, you are famous for wisdom.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante4");
	context.getTexLib()->createTextTexture("virgil4", "Virgil: \"The beasts so which you call out do not allow others to pass,\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil4");
	context.getTexLib()->createTextTexture("virgil5", "Virgil: \"but hold them, and in the end destroy them.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil5");
	context.getTexLib()->createTextTexture("virgil6", "Virgil: \"Beatrice, your former love, who has seen the curse laid upon you by\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil6");
	context.getTexLib()->createTextTexture("virgil7", "Virgil: \"your people, to be unable to interact with the living, has called from\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil7");
	context.getTexLib()->createTextTexture("virgil8", "Virgil: \"heaven for me to guide you to her to break your curse.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil8");
	context.getTexLib()->createTextTexture("virgil9", "Virgil: \"Before you can enter the gate of St Peter, your soul must be purified.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil9");
	context.getTexLib()->createTextTexture("virgil10", "Virgil: \"You'll have to go another way to bypass these beasts and enter hell\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil10");
	context.getTexLib()->createTextTexture("virgil11", "Virgil: \"to cleanse your soul. Go now and I will guide thee.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil11");
	context.getTexLib()->createTextTexture("virgilTip1", "Virgil: Turn back or the beast will devour you", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	context.getTexLib()->createTextTexture("virgilTip2", "Virgil: Turn back or the wolf will hunt you", 18, glm::vec3(0.0f, -0.9f, 0.0f), glm::vec3(255, 100, 0));
	context.getTexLib()->createTextTexture("virgilTip3", "Virgil: Turn back or the hellhound will claim your soul", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	
}

/*
* @param - game - the context of the game.
*/
void playState::enter(game &context)
{			
	lockControls = false;
	enterVirgil = false;
	keyTimer = clock();
}

/*
* 
* @param - SDL_Window - window to draw to.
* @param - game - game context.
*/
void playState::draw(SDL_Window * window, game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
		
	rt3d::lightStruct lightEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{3.0f, 3.0f, 3.0f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular		
		std::acos(90.0f),         // spotCosCutOff
		std::acos(100.0f)         // spotExponent
	};
	
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);	
	glm::mat4 modelview(1.0); // set base position for scene	
	glm::mat3 normalMatrix(1.0); //used for normal transformation

	//Set camera
	mvStack.push(modelview);	
	context.getGameCamera()->update(context.getGamePlayer()->getPos());
	mvStack.top() = glm::lookAt(context.getGameCamera()->getEye(), context.getGameCamera()->getAt(), context.getGameCamera()->getUp());
	
	//Draws skybox
	context.getSkyBox()->draw(mvStack.top(), projection);

	glDepthMask(GL_TRUE); // ensure depth test back on after skybox
			
	glUseProgram(shaderProgram);

	//sets light
	rt3d::setLight(shaderProgram, lightEnvironment);

	//set light position, coneDirection and attenuation
	lightPos = glm::vec4(context.getGameCamera()->getEye().x, context.getGameCamera()->getEye().y, context.getGameCamera()->getEye().z, 1.0f);
	glm::vec4 tmpLight = mvStack.top() * lightPos;
	rt3d::setUniformVector4fv(shaderProgram, "lightPosition", glm::value_ptr(tmpLight));
	coneDirection = glm::vec4(context.getGameCamera()->getAt().x, context.getGameCamera()->getAt().y, context.getGameCamera()->getAt().z, 1.0f);
	glm::vec4 tmpConeDirection = mvStack.top() * coneDirection;
	rt3d::setUniformVector4fv(shaderProgram, "coneDirection", glm::value_ptr(tmpConeDirection));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
		
	//set projection
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));	
	
	//draw objects
	context.getGameObjects()->draw(mvStack, context.getGameCamera(), shaderProgram);

	context.getGamePlayer()->draw(mvStack.top(), shaderProgram);

	context.getGameWolf()->draw(mvStack.top(), shaderProgram);

	context.getGameBeast()->draw(mvStack.top(), shaderProgram);

	context.getGameMonster()->draw(mvStack.top(), shaderProgram);
		
	if(enterVirgil == true)
		context.getGameVirgil()->draw(mvStack.top(), shaderProgram);

	mvStack.pop(); //last pop

	//Draw HUD			
	glDepthMask(GL_FALSE);
	glUseProgram(hudShader);
	rt3d::setUniformMatrix4fv(hudShader, "projection", glm::value_ptr(glm::mat4(1.0f)));
	
	glm::mat4 hudMV(1.0f);	
	glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("healthBar", "text")->getTexture());	
	hudMV = glm::translate(hudMV, glm::vec3(-0.85f, 0.95f, 0.0f));
	hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("healthBar", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("healthBar", "text")->getHeight()*0.004f, 0.0f));	
	hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
	rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	
	glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("black", "image")->getTexture());	
	hudMV = glm::mat4(1.0f);	
	hudMV = glm::translate(hudMV, glm::vec3(-0.52f, 0.94f, 0.0f));
	hudMV = glm::scale(hudMV,glm::vec3(0.4f, 0.1f, 1.0f));				
	rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	
	//Works out the scale and position of health bar based on players current health
	float healthScale = 0.0038 * context.getGamePlayer()->getHealth();
	float healthTranslate = -0.52 + (0.38 - healthScale)/2;
	
	glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("red", "image")->getTexture());	
	hudMV = glm::mat4(1.0f);
	hudMV = glm::translate(hudMV, glm::vec3(healthTranslate, 0.94f, 0.0f));	
	hudMV = glm::scale(hudMV,glm::vec3(healthScale, 0.08f, 1.0f));	
	rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		
	glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("playerDeaths", "text")->getTexture());	
	hudMV = glm::mat4(1.0f);
	hudMV = glm::translate(hudMV, glm::vec3(0.65f, 0.95f, 0.0f));
	hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("playerDeaths", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("playerDeaths", "text")->getHeight()*0.004f, 0.0f));	
	hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
	rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);

	//draw narrative text
	if(enterVirgil == true && narrativeText < narrativeNames.size())
	{
		if(clock() < narrativeTimer)
		{
			hudMV = glm::mat4(1.0f);
			glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj(narrativeNames[narrativeText], "text")->getTexture());	
			hudMV = glm::translate(hudMV, context.getTexLib()->getTextureObj(narrativeNames[narrativeText], "text")->getPosition());
			hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj(narrativeNames[narrativeText], "text")->getWidth()*0.003f,
			context.getTexLib()->getTextureObj(narrativeNames[narrativeText], "text")->getHeight()*0.004f, 0.0f));	
			hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
			rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
			rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		}
		else
		{
			//move on to next narrative speech
			narrativeTimer = clock() + 8000;
			narrativeText++;
			if(narrativeText == narrativeNames.size())
				lockControls = false;
		}
	}
	
	if(tip1 == true)
	{//draw tip1
		hudMV = glm::mat4(1.0f);
		glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("virgilTip1", "text")->getTexture());	
		hudMV = glm::translate(hudMV, context.getTexLib()->getTextureObj("virgilTip1", "text")->getPosition());
		hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("virgilTip1", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("virgilTip1", "text")->getHeight()*0.004f, 0.0f));	
		hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	}
	if(tip2 == true)
	{//draw tip2
		hudMV = glm::mat4(1.0f);
		glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("virgilTip2", "text")->getTexture());	
		hudMV = glm::translate(hudMV, context.getTexLib()->getTextureObj("virgilTip2", "text")->getPosition());
		hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("virgilTip2", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("virgilTip2", "text")->getHeight()*0.004f, 0.0f));	
		hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	}
	if(tip3 == true)
	{//draw tip3
		hudMV = glm::mat4(1.0f);
		glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("virgilTip3", "text")->getTexture());	
		hudMV = glm::translate(hudMV, context.getTexLib()->getTextureObj("virgilTip3", "text")->getPosition());
		hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("virgilTip3", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("virgilTip3", "text")->getHeight()*0.004f, 0.0f));	
		hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	}

	glDepthMask(GL_TRUE);

	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - game - the context of the game.
*/
void playState::update(game &context)
{		
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	
	if(lockControls == false)
	{	
		//player controls
		//moves player, if collision occurs, moves player in reverse direction
		//from what key was pressed.
		if ( keys[SDL_SCANCODE_W] )
		{
			context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_S] )
		{
			context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_A] )
		{
			context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_D] ) 
		{
			context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
		}
	}
	//camera controls
	//rotates camera, if collision occurs, rotates reverse direction
	//from what key was pressed.
	if ( keys[SDL_SCANCODE_Q] || keys[SDL_SCANCODE_LEFT] )
	{
		context.getGameCamera()->rotateLeft();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateRight();	
	}
	if ( keys[SDL_SCANCODE_E] || keys[SDL_SCANCODE_RIGHT] ) 
	{
		context.getGameCamera()->rotateRight();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateLeft();				
	}
	if ( keys[SDL_SCANCODE_R] || keys[SDL_SCANCODE_UP] )
	{	
		context.getGameCamera()->lookUp();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookDown();				
	}
	if ( keys[SDL_SCANCODE_F] || keys[SDL_SCANCODE_DOWN] ) 
	{
		context.getGameCamera()->lookDown();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookUp();				
	}	
	//skip current text	
	if ( keys[SDL_SCANCODE_SPACE] && enterVirgil == true && clock() > keyTimer)
	{
		keyTimer = clock() + 1000;
		narrativeText++;
		narrativeTimer = clock() + 8000;			
		if(narrativeText >= narrativeNames.size())
				lockControls = false;
	}

	//if player dies reset to try again
	if(context.getGamePlayer()->getHealth() == 0)
	{
		context.getGamePlayer()->reset();
		context.getGameCamera()->setRotation(0);
		if(context.getGamePlayer()->getDeaths() == 10)
			context.setState(context.getEndState());
		context.getTexLib()->deleteTexture("playerDeaths", "text");
		std::stringstream str;
		str << "Deaths: " << context.getGamePlayer()->getDeaths();	
		context.getTexLib()->createTextTexture("playerDeaths", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));	
	}

	//Virgil's entrance
	if(context.getGamePlayer()->getHealth() < 50 && enterVirgil == false)
	{
		context.getGamePlayer()->reset();
		enterVirgil = true;
		lockControls = true;
		context.getGameCamera()->setRotation(225.0f);
		context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());
		narrativeTimer = clock() + 10000;
		narrativeText++;
	}
	//update Virgil's aabb
	if(enterVirgil == true)
		context.getGameVirgil()->seek(*context.getGamePlayer()->getAABB());


	//Update NPC actions - dependent on player's location
	context.getGameBeast()->update(*context.getGamePlayer());
	context.getGameWolf()->update(*context.getGamePlayer());
	context.getGameMonster()->update(*context.getGamePlayer());

	//Check if player in territory, if is give tip from Virgil
	if(enterVirgil == true)
	{
		if(context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameBeast()->getBoundaryAABB()) == true)
			tip1 = true;
		else
			tip1 = false;
		if(context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameWolf()->getBoundaryAABB()) == true)
			tip2 = true;
		else
			tip2 = false;

		glm::vec3 min = glm::vec3(20.0f, 0.0f, -20.0f);
		glm::vec3 max = glm::vec3(60.0f, 10.0f, 0.0f);
		if((context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameMonster()->getBoundaryAABB()) == true)
			|| !(max.x <= context.getGamePlayer()->getAABB()->getMin().x || max.y <= context.getGamePlayer()->getAABB()->getMin().y || max.z <= context.getGamePlayer()->getAABB()->getMin().z 
				|| min.x >= context.getGamePlayer()->getAABB()->getMax().x || min.y >= context.getGamePlayer()->getAABB()->getMax().y || min.z >= context.getGamePlayer()->getAABB()->getMax().z))
			tip3 = true;
		else
			tip3 = false;		
	}

	//Check if player reached end of level
	playerEndCollision(context);	
}

/*
* Returns if player has collided with an object - current ignores billboards and ground.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::playerObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "ground" && context.getGameObjects()->getObject(i)->getType() != "billboard")
			if(context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)
				return true;		
	return false;		
}

/*
* Returns if camera has collided with an object - current ignores billboards.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::cameraObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "billboard")
			if(context.getGameCamera()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)			
				return true;		
	return false;
}

/*
* Sets new state if player has reached exit for level.
* @param - game - the game context.
*/
void playState::playerEndCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x < -45.0f && context.getGamePlayer()->getPos().x > -50.0f
		&& context.getGamePlayer()->getPos().z > 50.0f && context.getGamePlayer()->getPos().z < 60.0f)
			context.setState(context.getEndState());
	
}

/*
* @param - game - the context of the game.
*/
void playState::exit(game &context)
{		
}