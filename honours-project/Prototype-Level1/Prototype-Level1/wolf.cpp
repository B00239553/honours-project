//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "game.h"
#include "wolf.h"

/*
* Constructor.
*/
wolf::wolf(void)
{

}

/*
* Variable Constructor.
* @param - std::string - path of model file.
*/
wolf::wolf(std::string path)
{
	modelPath = path;
	velocity = 0.1f;
	position = glm::vec3(-40, -2.5f, -20.0f);
	rotate = glm::vec3(0.0f, 1.0f, 0.0f);
	rotation = 0.0f;
	modelRotation = 180.0f;
}

/*
* Initialises wolf model.
*/
void wolf::init(void)
{
	NPCmodel = new model(modelPath);
	NPCmodel->init();

	//Sets NPC territory
	std::vector<glm::vec3> boundaryVerts;
	boundaryVerts.push_back(glm::vec3(-60.0f, 0.0f, -60.0f));
	boundaryVerts.push_back(glm::vec3(-20.0f, 10.0f, 20.0f));
	boundary = new AABB();
	boundary->setAABBvec3(boundaryVerts);
	aabb = new AABB();
	aabbSet = false;

	timer = clock() + 2000;
	movePath = rand() % 100;
}

/*
* Required to move the wolf forwards and backwards.
* @param - glm::vec3 - the position of the wolf.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 wolf::movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance)
{
	return glm::vec3(pos.x + distance*std::sin(angle*DEG_TO_RAD), pos.y, pos.z - distance*std::cos(angle*DEG_TO_RAD));
}

/*
* Required to move the wolf left and right.
* @param - glm::vec3 - the position of the wolf.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 wolf::movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance)
{
	return glm::vec3(pos.x + distance*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + distance*std::sin(angle*DEG_TO_RAD));
}

/*
* Moves wolf forward.
* If movement will move wolf out of boundary, reverses movement.
*/
void wolf::moveForward()
{
	position = movementForward(position, rotation, velocity);
	if(!((position.x - aabb->getLength()/2) > boundary->getMin().x && (position.x + aabb->getLength()/2) < boundary->getMax().x 
		&& (position.z - aabb->getDepth()/2) > boundary->getMin().z && (position.z + aabb->getDepth()/2) < boundary->getMax().z))		
			position = movementForward(position, rotation, -velocity);	
}

/*
* Moves wolf back.
* If movement will move wolf out of boundary, reverses movement.
*/
void wolf::moveBack()
{
	position = movementForward(position, rotation, -velocity);
	if(!((position.x - aabb->getLength()/2) > boundary->getMin().x && (position.x + aabb->getLength()/2) < boundary->getMax().x 
		&& (position.z - aabb->getDepth()/2) > boundary->getMin().z && (position.z + aabb->getDepth()/2) < boundary->getMax().z))		
			position = movementForward(position, rotation, velocity);
	else
	{
		modelRotation = 0.0f;
	}
}

/*
* Moves wolf right.
* If movement will move wolf out of boundary, reverses movement.
*/
void wolf::moveRight()
{
	position = movementRight(position, rotation, velocity);
	if(!((position.x - aabb->getLength()/2) > boundary->getMin().x && (position.x + aabb->getLength()/2) < boundary->getMax().x 
		&& (position.z - aabb->getDepth()/2) > boundary->getMin().z && (position.z + aabb->getDepth()/2) < boundary->getMax().z))		
			position = movementRight(position, rotation, -velocity);
	else
	{
		modelRotation = 90.0f;
	}
}

/*
* Moves wolf left.
* If movement will move wolf out of boundary, reverses movement.
*/
void wolf::moveLeft()
{
	position = movementRight(position, rotation, -velocity);
	if(!((position.x - aabb->getLength()/2) > boundary->getMin().x && (position.x + aabb->getLength()/2) < boundary->getMax().x 
		&& (position.z - aabb->getDepth()/2) > boundary->getMin().z && (position.z + aabb->getDepth()/2) < boundary->getMax().z))		
			position = movementRight(position, rotation, velocity);
	else
	{
		modelRotation = 270.0f;
	}
}

/*
* Moves wolf in random path based on clock time and
* value in range from 0 - 99.
*/
void wolf::patrol(void)
{
	if(timer < clock())
	{
		timer = clock() + 2000;
		movePath = rand() % 100;
	}
	else
	{
		if(movePath >= 0 && movePath <= 19)
			moveForward();
		if(movePath >= 20 && movePath <= 39)
			moveBack();
		if(movePath >= 40 && movePath <= 59)
			moveRight();
		if(movePath >= 60 && movePath <= 79)
			moveLeft();
	}
}

/*
* Seeks player as in territory.
* @param - glm::vec3 - player position.
*/
void wolf::seek(glm::vec3 pos)
{
	glm::vec3 NPCToPos = glm::vec3(pos.x - position.x, 0.0f, pos.z - position.z);	//new look at vector
	glm::vec3 modelLookAt = glm::vec3(0.0f, 0.0f, 1.0f); //model look at vector
	//calculates polar coordinates to find angle NPC should look at
	float dotModelPos = glm::dot(modelLookAt, NPCToPos);
	float modelMag = sqrt(modelLookAt.x*modelLookAt.x + modelLookAt.y*modelLookAt.y + modelLookAt.z*modelLookAt.z);
	float NPCToPosMag = sqrt(NPCToPos.x*NPCToPos.x + NPCToPos.y*NPCToPos.y + NPCToPos.z*NPCToPos.z);
	modelRotation = std::acos(dotModelPos/(modelMag*NPCToPosMag)) * RAD_TO_DEG;	
	rotate = glm::cross(modelLookAt, NPCToPos);			
	if(rotate.y < 0) //change player rotation based on cross to know whether angle is positive or negative
		rotation = modelRotation + 180;
	else
		rotation = -modelRotation + 180;
	moveForward();			
}

/*
* Inflicts damage on player. 
* Rotation updated to ensure looking at player when attacks.
* @param - player - player in game.
*/
void wolf::attack(player &gamePlayer)
{
	glm::vec3 NPCToPos = glm::vec3(gamePlayer.getPos().x - position.x, 0.0f, gamePlayer.getPos().z - position.z);	//new look at vector
	glm::vec3 modelLookAt = glm::vec3(0.0f, 0.0f, 1.0f); //model look at vector
	//calculates polar coordinates to find angle NPC should look at
	float dotModelPos = glm::dot(modelLookAt, NPCToPos);
	float modelMag = sqrt(modelLookAt.x*modelLookAt.x + modelLookAt.y*modelLookAt.y + modelLookAt.z*modelLookAt.z);
	float NPCToPosMag = sqrt(NPCToPos.x*NPCToPos.x + NPCToPos.y*NPCToPos.y + NPCToPos.z*NPCToPos.z);
	modelRotation = std::acos(dotModelPos/(modelMag*NPCToPosMag)) * RAD_TO_DEG;	
	rotate = glm::cross(modelLookAt, NPCToPos);			
	if(rotate.y < 0) //change player rotation based on cross to know whether angle is positive or negative
		rotation = modelRotation + 180;
	else
		rotation = -modelRotation + 180;
	gamePlayer.damage(24);
}

/*
* Will change wolf's action between, patrol, seek and attack.
* @param - player - game player, used to know what action to take.
*/
void wolf::update(player &gamePlayer)
{		
	aabb->updateAABB(position);
	if(gamePlayer.getPos().x > boundary->getMin().x && gamePlayer.getPos().x < boundary->getMax().x 
		&& gamePlayer.getPos().z > boundary->getMin().z && gamePlayer.getPos().z < boundary->getMax().z)
	{
		if(aabb->AABBcollision(*gamePlayer.getAABB()) == true)
		{
			if(timer < clock())
			{
				attack(gamePlayer);
				timer = clock() + 2000;
			}
		}
		else if(timer < clock())
		{
			velocity = 0.5f;
			seek(gamePlayer.getPos());
		}
	}
	else
	{
		velocity = 0.1f;
		rotation = 0.0f;
		rotate = glm::vec3(0.0f, 1.0f, 0.0f);
		modelRotation = 180.0f;
		patrol();
	}	
}

/*
* Draws wolf.
* @param - glm::mat4 - the current modelview.
* @param - GLuint - shader program to be used.
*/
void wolf::draw(glm::mat4 modelview, GLuint shader)
{
	glUseProgram(shader);
	glCullFace(GL_BACK);
	modelview = glm::translate(modelview, position);
	modelview = glm::rotate(modelview, modelRotation, rotate);
	modelview = glm::scale(modelview, glm::vec3(1.5f, 1.5f, 1.5f));		
	if(aabbSet == false)
	{	
		glm::mat4 colMat(1.0f);
		colMat = glm::translate(colMat, position);
		colMat = glm::rotate(colMat, modelRotation, glm::vec3(0.0f, 1.0f, 0.0f));
		colMat = glm::scale(colMat, glm::vec3(1.5f, 1.5f, 1.5f));
		std::vector<glm::vec3> tmpVerts = NPCmodel->getVerts();			
		for(int i = 0; i < tmpVerts.size(); i++)
		{
			glm::vec4 tempVec = colMat * glm::vec4(tmpVerts[i].x, tmpVerts[i].y, tmpVerts[i].z, 1.0);
			tmpVerts[i].x = tempVec.x;
			tmpVerts[i].y = tempVec.y;
			tmpVerts[i].z = tempVec.z;	
		}
		aabb->setAABBvec3(tmpVerts);	
		aabbSet = true;
	}	
	glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelview)));	
	rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	NPCmodel->draw(shader);
	glCullFace(GL_FRONT);
}