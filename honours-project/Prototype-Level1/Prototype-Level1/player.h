//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef PLAYER_H
#define PLAYER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "model.h"
#include "AABB.h"

#define DEG_TO_RAD 0.017453293

/*
* Player Class - Will create player, load model and draw player.
*/
class player
{
private:
	glm::vec3 position;
	glm::vec3 modelCentre;
	glm::vec3 leftShoulder;
	glm::vec3 rightShoulder;
	GLfloat rotation;
	GLfloat modelRotation;
	int health;
	int deaths;
	float velocity;
	std::string modelPath;
	model * playerModel;
	AABB * aabb;
	bool aabbSet;
	glm::vec3 movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance);
	glm::vec3 movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance);
public:
	player(void);
	~player(void);
	player(std::string mPath);
	void init(void);
	void reset(void);
	int getHealth(void) {return health;}
	int getDeaths(void) {return deaths;}	
	glm::vec3 getPos(void) {return position;}
	GLfloat getRot(void) {return rotation;}
	AABB* getAABB(void) {return aabb;}	
	void setPos(glm::vec3 pos) {position = pos;}
	void damage(int damTaken);
	void moveForward(float angle);
	void moveBack(float angle);
	void moveRight(float angle);
	void moveLeft(float angle);
	void draw(glm::mat4 modelview, GLuint shader);
};

#endif PLAYER_H