//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef VIRGIL_H
#define VIRGIL_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "model.h"
#include "AABB.h"

#define DEG_TO_RAD 0.017453293
#define RAD_TO_DEG 57.29577951f

/*
* Virgil Class - Will create virgil, load model and draw virgil.
* Will also control movement actions
*/
class virgil
{
private:
	glm::vec3 position;
	glm::vec3 rotate;
	glm::vec3 modelCentre;
	GLfloat rotation;
	GLfloat modelRotation;	
	float velocity;
	std::string modelPath;
	model * virgilModel;
	AABB * aabb;
	bool aabbSet;
	glm::vec3 movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance);
	glm::vec3 movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance);
public:
	virgil(void);
	~virgil(void);
	virgil(std::string mPath);
	void init(void);	
	void moveForward(void);
	void moveBack(void);
	void moveLeft(void);
	void moveRight(void);
	void seek(AABB dante);
	AABB* getAABB(void) {return aabb;}		
	void draw(glm::mat4 modelview, GLuint shader);
};

#endif VIRGIL_H