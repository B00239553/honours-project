//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "AABB.h"

#ifndef CAMERA_H
#define CAMERA_H

#define DEG_TO_RAD 0.017453293

/*
* Camera class for game world.
*/
class camera
{
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
	GLfloat rotation;
	GLfloat orientation;
	AABB * aabb;
	glm::vec3 moveForward(glm::vec3 pos, GLfloat angle, GLfloat orientation, GLfloat d);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);
public:
	camera(void);
	camera(glm::vec3 mEye, glm::vec3 mAt, glm::vec3 mUp);
	~camera(void);
	void init(void);
	glm::vec3 getEye(void) {return eye;}
	glm::vec3 getAt(void) {return at;}
	glm::vec3 getUp(void) {return up;}
	GLfloat getRotation(void) {return rotation;}
	GLfloat getOrientation(void) {return orientation;}
	AABB* getAABB(void) {return aabb;}
	void moveForward(void);
	void moveBack(void);
	void moveLeft(void);
	void moveRight(void);
	void rotateLeft(void);
	void rotateRight(void);
	void lookUp(void);
	void lookDown(void);
	void setEye(glm::vec3 mEye) {eye = mEye; aabb->updateAABB(eye);}
	void setRotation(GLfloat rot) {rotation = rot;}
	void update(glm::vec3 pos);
};

#endif