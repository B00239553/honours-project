//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "AABB.h"

/*
* Constructor.
*/
AABB::AABB(void)
{
	min = glm::vec3(99999.99, 99999.99, 99999.99);
	max = glm::vec3(-99999.99, -99999.99, -99999.99);
}

/*
* Constructor.
* @param - std::string - name of bounding, should match its object.
*/
AABB::AABB(std::string mName)
{	
	name = mName;	
	min = glm::vec3(99999.99, 99999.99, 99999.99);
	max = glm::vec3(-99999.99, -99999.99, -99999.99);
}

/*
*  Deconstructor
*/
AABB::~AABB(void)
{	
} 

/*
*  Sets min and max points for AABB using vector of floats.
*  Sets length, width and height.
*  @param - vector - <GLfloat> takes in verts of object from a vector.
*  @param - int - takes in vertex count for object.
*/
void AABB::setAABB(const std::vector<GLfloat> &verts, int vertCount)
{		
	for(int i = 0; i < vertCount; i += 3)
	{
		if( verts[i] < min.x ) min.x = verts[i];
		if( verts[i + 1] < min.y ) min.y = verts[i + 1];
		if( verts[i + 2] < min.z ) min.z = verts[i + 2];
 
		if( verts[i] > max.x ) max.x = verts[i];
		if( verts[i + 1] > max.y ) max.y = verts[i + 1];
		if( verts[i + 2] > max.z ) max.z = verts[i + 2];
	}
	
	//Set Length, Height & Depth based on differences between min/max points.
	length = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;	
}

/*
*  Sets min and max points for AABB using vector of vec3.
*  Sets length, width and height.
*  @param - vector<glm::vec3> - takes in verts of object from a vector.
*/
void AABB::setAABBvec3(const std::vector<glm::vec3> &verts)
{
	for(int i = 0; i < verts.size(); i++)
	{
		if( verts[i].x < min.x ) min.x = verts[i].x;
		if( verts[i].y < min.y ) min.y = verts[i].y;
		if( verts[i].z < min.z ) min.z = verts[i].z;
 
		if( verts[i].x > max.x ) max.x = verts[i].x;
		if( verts[i].y > max.y ) max.y = verts[i].y;
		if( verts[i].z > max.z ) max.z = verts[i].z;
	}

	//Set Length, Height & Depth based on differences between min/max points.
	length = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;	
}

/*
*  Used to update the centre in respect to the objects centre. 
*  Will set the new min and max based on the AABB centre and
*  the dimensions of the box.
*  @param - glm::vec3 - vector taken in to position the bounding box.
*/
void AABB::updateAABB(glm::vec3 pos)
{
	centre = pos;
	min = glm::vec3(pos.x - (length/2), pos.y - (height/2), pos.z - (depth/2));
	max = glm::vec3(pos.x + (length/2), pos.y + (height/2), pos.z + (depth/2));	
}

/*
*  Checks if this AABB intersects with AABB taken in.
*  @param - AABB - used for intersection test.
*  @return - boolean - returns true if bounding boxes intersect, false
*  otherwise.
*/
bool AABB::AABBcollision(AABB other)
{
	if(max.x <= other.min.x || max.y <= other.min.y || max.z <= other.min.z 
	|| min.x >= other.max.x || min.y >= other.max.y || min.z >= other.max.z)	
		return false;
	else
		return true;
}