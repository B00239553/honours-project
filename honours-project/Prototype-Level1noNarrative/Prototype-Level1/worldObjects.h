//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef WORLDOBJECTS_H  
#define WORLDOBJECTS_H

#define RAD_TO_DEG 57.29577951f

#include "textureLibrary.h"
#include "camera.h"
#include "rt3d.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <stack>
#include "object.h"
#include "AABB.h"

/*
* Class to create and draw the world objects.
*/
class worldObjects
{
private:
	std::vector<object*> sceneObjects;		
	GLuint cubeObject;
	GLuint cubeIndex;	
	GLuint bbObject;
	GLuint bbIndex;
	bool AABBset;
	std::vector<GLfloat> cubeVerts;	
	std::vector<GLfloat> bbVerts;
public:
	worldObjects(void);
	worldObjects(std::vector<GLfloat> cubeV, GLuint cubeMesh, GLuint cubeCount, std::vector<GLfloat> bbV, GLuint bbMesh, GLuint bbCount);
	~worldObjects(void);
	void init(textureLibrary &texLib);
	int getNumObjects(void) {return sceneObjects.size();}
	object *getObject(int obj) {return sceneObjects[obj];}		
	void deleteAllObjects(void);
	void draw(std::stack<glm::mat4> mvStack, camera *gameCam, GLuint shader);	
};

#endif