//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "mesh.h"

/*
* Constructor.
*/
mesh::mesh(void)
{

}

/*
* Deconstructor.
*/
mesh::~mesh(void)
{

}

/*
* Variable Construtor.
* @param - std::vector<Vertex> - vector to hold mesh vertex structs.
* @param - std::vector<GLuint> - vector to hold mesh indices.
* @param - std::vector<Texture> - vector to hold mesh texture structs.
*/
mesh::mesh(std::vector<Vertex> verts, std::vector<GLuint> inds, std::vector<Texture> texs)
{
	vertices = verts;
	indices = inds;
	textures = texs;	
	setUpMesh();
}

/*
* Sets up VAO, VBO and EBO.
*/
void mesh::setUpMesh(void)
{	
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

	//Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	//Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	//Vertex Texture Coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));

	glBindVertexArray(0);
}

/*
* Returns a vector that holds the mesh vertices.
* @return - glm::vec3 - vertice coordinates to be returned.
*/
std::vector<glm::vec3> mesh::getVerts(void)
{
	std::vector<glm::vec3> verts;
	for(int i = 0; i < vertices.size(); i++)
	{
		verts.push_back(vertices[i].position);
	}
	return verts;
}

/*
* Draws mesh.
* @param - GLuint - shader program to be used for mesh.
*/
void mesh::draw(GLuint shader)
{	
	GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    for(GLuint i = 0; i < this->textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
        // Retrieve texture number (the N in diffuse_textureN)
        std::stringstream ss;
        std::string number;
        std::string name = this->textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++; // Transfer GLuint to stream
        else if(name == "texture_specular")
            ss << specularNr++; // Transfer GLuint to stream
        number = ss.str(); 

        glUniform1f(glGetUniformLocation(shader, ("material." + name + number).c_str()), i);
        glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
    }
    glActiveTexture(GL_TEXTURE0);

    // Draw mesh
    glBindVertexArray(this->VAO);
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}