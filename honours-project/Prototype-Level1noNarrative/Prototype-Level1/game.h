//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "rt3d.h"
#include "objectLoader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include <stdlib.h>
#include <ctime>
#include "NPC.h"
#include "textureLibrary.h"
#include "gameState.h"
#include "skybox.h"
#include "camera.h"
#include "worldObjects.h"
#include "player.h"

#ifndef GAME_H  
#define GAME_H

/*
* game Class - used to load everything and change states.
*/
class game
{
private:		
	SDL_Window * hWindow; 
    SDL_GLContext glContext;
	
	GLuint cubeMeshObject;
	GLuint cubeIndexCount;
	GLuint billboardMeshObject;
	GLuint billboardIndexCount;

	GLuint objectShader;
	GLuint textShader;
	GLuint cubeMapShader;

	textureLibrary * texLib;

	gameState * currentState;
	gameState * State_Intro;
	gameState * State_Play;
	gameState * State_End;

	skybox * sky_Box;
	camera * game_Camera;
	worldObjects * game_Objects;
	player * game_Player;
	NPC * game_Wolf;
	NPC * game_Beast;
	NPC * game_Monster;

	bool gameRunning;	
public:	
	game(void); 	
	~game(void);
	void init(void);
	SDL_Window * setupRC(SDL_GLContext &context);
	textureLibrary* getTexLib(void) {return texLib;}
	gameState* getState(void) {return currentState;}
	gameState* getIntroState(void) {return State_Intro;}
	gameState* getPlayState(void) {return State_Play;}
	gameState* getEndState(void) {return State_End;}
	skybox* getSkyBox(void) {return sky_Box;}
	camera* getGameCamera(void) {return game_Camera;}
	worldObjects* getGameObjects(void) {return game_Objects;}
	player* getGamePlayer(void) {return game_Player;}
	NPC* getGameWolf(void) {return game_Wolf;}
	NPC* getGameBeast(void) {return game_Beast;}
	NPC* getGameMonster(void) {return game_Monster;}
	void setState(gameState* newState);
	void setGameRunning(bool set) {gameRunning = set;}	
	void runGame(void);		
};

#endif