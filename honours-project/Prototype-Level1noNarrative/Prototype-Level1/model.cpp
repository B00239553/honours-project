//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include "model.h"

/*
* Constructor.
*/
model::model()
{

}

/*
* Deconstructor.
*/
model::~model(void)
{

}

/*
* Variable Constructor.
* @param - std::string - file path for object to be loaded.
*/
model::model(std::string path)
{
	directory = path;
}

/*
* Initialises model by calling private loadModel function.
*/
void model::init(void)
{
	loadModel(directory);
}

/*
* Loads model using assimp, will call other functions
* required to process mesh.
* @param - std::string - file path name.
*/
void model::loadModel(std::string path)
{
	Assimp::Importer import;
	const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	//if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode);	
	//	std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;	
	std::cout << "Loading Model." << std::endl;
	directory = path.substr(0, path.find_last_of('/'));
	processNode(scene->mRootNode, scene);
}

/*
* Processes the nodes held for the object loaded.
* @param - aiNode* - assimp nodes to access meshes.
* @param - aiScene - assimp scene object which holds complete model.
*/
void model::processNode(aiNode* node, const aiScene* scene)
{
	for(int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* newMesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(newMesh, scene));
	}
	for(int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}
	
/*
* Processes mesh and creates mesh class for model.
* @param - aiMesh* - assimp mesh - used to create mesh class.
* @param - aiScene* - assimp scene object which holds complete model.
*/
mesh model::processMesh(aiMesh* meshIn, const aiScene* scene)
{	
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	for(int i = 0; i < meshIn->mNumVertices; ++i)
	{
		Vertex vertex;

		glm::vec3 vector;
		vector.x = meshIn->mVertices[i].x;
		vector.y = meshIn->mVertices[i].y;
		vector.z = meshIn->mVertices[i].z;
		vertex.position = vector;

		vector.x = meshIn->mNormals[i].x;
		vector.y = meshIn->mNormals[i].y;
		vector.z = meshIn->mNormals[i].z;
		vertex.normal = vector;

		if(meshIn->mTextureCoords[0]) 
		{
			glm::vec2 vec;
			vec.x = meshIn->mTextureCoords[0][i].x;
			vec.y = meshIn->mTextureCoords[0][i].y;
			vertex.texCoord = vec;
		}
		else
		{
			std::cout << "Model has no texture coordinates." << std::endl;
			vertex.texCoord = glm::vec2(0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}

	for(int i = 0; i < meshIn->mNumFaces; i++)
	{
		aiFace face = meshIn->mFaces[i];
		for(int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}

	if(meshIn->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[meshIn->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	return mesh(vertices, indices, textures);
}

/*
* Loads textures for model.
* @param - aiMaterial* - assimp material type.
* @param - std::string - name of material type to be loaded.
*/
std::vector<Texture> model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for(int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;		
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for(int j = 0; j < textures_loaded.size(); j++)
		{
			if(textures_loaded[j].filePath == str)
			{
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}
		if(!skip)
		{			
			Texture texture;
			texture.id = loadPNG(str.C_Str(), directory);			
			texture.type = typeName;
			texture.filePath = str;
			textures.push_back(texture);
			textures_loaded.push_back(texture);
		}
	}
	return textures;
}

/*
* Loads PNG file for model textures. 
* Used outside texture library as connected to loading model.
* @param - const char* - file name to be loaded.
* @param - std::string - path file where file name is located.
*/
GLuint model::loadPNG(const char* fileName, std::string dir)
{
	GLuint tex;
	glGenTextures(1, &tex); 	
	std::string str = std::string(fileName);
	str = dir + '/' + str;

	// load file - using core SDL library 	
	SDL_Surface* loadedSurface = IMG_Load(str.c_str());
	if ( !loadedSurface )
    {
		printf ( "IMG_Load: %s\n", IMG_GetError () );      
    }
			
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = loadedSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,loadedSurface->w, loadedSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, loadedSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(loadedSurface); 
	return tex;
}

/*
* Returns a vector that holds the model vertices.
* @return - glm::vec3 - vertice coordinates to be returned.
*/
std::vector<glm::vec3> model::getVerts(void)
{
	std::vector<glm::vec3> verts;
	for(int i = 0; i < meshes.size(); i++)
	{
		std::vector<glm::vec3> tmpVerts = meshes[i].getVerts();
		for(int j = 0; j < tmpVerts.size(); j++)
		{
			verts.push_back(tmpVerts[j]);
		}		
	}
	return verts;
}

/*
* Draws meshes for model.
* @param - GLuint - shader to be used.
*/
void model::draw(GLuint shader)
{
	for(int i = 0; i < this->meshes.size(); i++)
        meshes[i].draw(shader);
}

/*
* Draws meshes for player model.
* Introduced as a quick method to rotate player arms out of T shape.
* @param - GLuint - shader to be used.
* @param - glm::mat4 - normal modelview.
* @param - glm::mat4 - modelview for left arm.
* @param - glm::mat4 - modelview for right arm.
*/
void model::playerDraw(GLuint shader, glm::mat4 mv, glm::mat4 leftArm, glm::mat4 rightArm)
{
	for(int i = 0; i < this->meshes.size(); i++)
	{
		if(i == 8 || i ==16 || i ==25 || i == 26)
		{
			glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(leftArm)));	
			rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));
			rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(leftArm));
		}
		else if(i == 9 || i == 12 || i == 22 || i == 24)
		{
			glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(rightArm)));	
			rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));
			rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(rightArm));
		}
		else	
		{
			glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(mv)));	
			rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));
			rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mv));
		}		
        meshes[i].draw(shader);
	}
}