//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

class game;

#include <ctime>
#include "model.h"
#include "AABB.h"
#include "player.h"

/*
* Abstract NPC class - NPCs will inherit from this class.
*/
class NPC
{
protected:
	glm::vec3 position;
	glm::vec3 rotate;
	GLfloat rotation;
	GLfloat modelRotation;
	float velocity;
	int movePath;
	clock_t timer;
	std::string modelPath;
	model * NPCmodel;
	AABB * aabb;
	bool aabbSet;
	AABB * boundary;
	virtual glm::vec3 movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance) = 0;
	virtual glm::vec3 movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance) = 0;
	virtual void moveForward() = 0;
	virtual void moveBack() = 0;
	virtual void moveRight() = 0;
	virtual void moveLeft() = 0;
	virtual void patrol(void) = 0;
	virtual void seek(glm::vec3 pos) = 0;
	virtual void attack(player &gamePlayer) = 0;
public:
	virtual ~NPC(void) { delete NPCmodel; return; }
	virtual void init(void) = 0;
	virtual AABB *getAABB(void) = 0;
	virtual void update(player &gamePlayer) = 0;
	virtual void draw(glm::mat4 modelview, GLuint shader) = 0;
};
