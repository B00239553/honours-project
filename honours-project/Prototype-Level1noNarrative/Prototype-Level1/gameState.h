//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#include <stack>

class game;

/*
* Abstract Game State Class.
* Different game states inherit from this.
*/
class gameState {
protected:
	std::stack<glm::mat4> mvStack; 	
	GLuint shaderProgram; 	
	GLuint meshObject;
	GLuint meshIndexCount;
public:	
	virtual ~gameState(void) { return; }
	virtual void draw(SDL_Window * window, game &context) = 0;
	virtual void init(game &context) = 0;
	virtual void update(game &context) = 0;	
	virtual void enter(game &context) = 0;
	virtual void exit(game &context) = 0;	
};