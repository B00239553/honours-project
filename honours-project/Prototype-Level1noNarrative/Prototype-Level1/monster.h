//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef MONSTER_H
#define MONSTER_H

/*
* Monster class - creates, draws and controls leopard NPC.
*/
class monster : public NPC
{
private:
	AABB * boundary2;
protected:	
	glm::vec3 movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance);
	glm::vec3 movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance);
	void moveForward();
	void moveBack();
	void moveRight();
	void moveLeft();
	void patrol(void);
	void seek(glm::vec3 pos);
	void attack(player &gamePlayer);
public:
	monster(void);
	monster(std::string path);
	void init(void);
	AABB *getAABB(void) {return aabb;}
	void update(player &gamePlayer);
	void draw(glm::mat4 modelview, GLuint shader);
};

#endif