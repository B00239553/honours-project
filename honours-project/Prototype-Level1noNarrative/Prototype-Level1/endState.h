//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef ENDSTATE_H   
#define ENDSTATE_H   

/*
* End State - Displays prototype completed.
*/
class endState : public gameState {
private:
	float scaleText;
	std::vector<std::string> textureNames;	
public:			
	endState(void);	
	endState(GLuint mesh, GLuint count, GLuint shader);
	~endState(void);
	void draw(SDL_Window * window, game &context);	
	void init(game &context);	
	void update(game &context);	
	void enter(game &context);
	void exit(game &context);		
};

#endif 