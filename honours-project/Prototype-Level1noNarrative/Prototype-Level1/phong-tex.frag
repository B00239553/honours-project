//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

//phong-tex.frag.
//Fragment shader to use with vertex shader.
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float spotCosCutOff;
	float spotExponent;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform vec4 coneDirection;
uniform vec3 attenuation;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform sampler2D textureUnit1;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;

layout(location = 0) out vec4 out_color;
 
void main(void) {
    
	// Light Direction
	vec3 lightDirection = ex_L;

	// Light Distance
	float lightDistance = distance(ex_L, ex_V);

	// Attenuation
	float A = 1.0 / (attenuation.x + attenuation.y * lightDistance + attenuation.z * lightDistance * lightDistance);

	// Normalise Light
	lightDirection = normalize(lightDirection);

	// Are we in the spot
	vec3 cD = normalize(vec3(coneDirection));
	float spotCos = dot(lightDirection, -cD);

	// Attenuate more based on spot-relative position
	if(spotCos < light.spotCosCutOff)
		A = 0.0;
	else
		A *= pow(spotCos, light.spotExponent);

	// Ambient intensity 
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(ex_N, lightDirection),0);
		
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(-lightDirection,ex_N));
	
	// Specular intensity
	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);
		
	// Fragment color	
	vec4 litColour = ambientI + (diffuseI * A) + (specularI * A);
	vec4 texel = texture(textureUnit0, ex_TexCoord) * litColour;	

	//discards pixels with low alpha
	if(texel.a < 0.5)
		discard;

	out_color = texel;
}