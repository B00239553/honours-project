//(c) Michael O'Neil 2015
//Dante Prototype - Level 1

#ifndef INTROSTATE_H   
#define INTROSTATE_H   

/*
* Intro State - Displays prototype name until key selected.
*/
class introState : public gameState
{
private:
	float scaleText;
	std::vector<std::string> textureNames;	
public:			
	introState(void);	
	introState(GLuint mesh, GLuint count, GLuint shader);
	~introState(void);
	void draw(SDL_Window * window, game &context);	
	void init(game &context);	
	void update(game &context);	
	void enter(game &context);
	void exit(game &context);		
};

#endif 