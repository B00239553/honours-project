//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "player.h"

/*
* Constructor.
*/
player::player(void)
{
}

/*
* Deconstructor.
*/
player::~player(void)
{
	delete playerModel;
}

/*
* Takes in file path for model to be loaded.
*/
player::player(std::string mPath)
{	
	rotate = glm::vec3(0.0f, 1.0f, 0.0f);
	fall = false;
	//modelCentre, leftShoulder & rightShoulder were pre-computed.
	//This is not the best method but has been included to change
	//player from T position that is loaded in from.
	modelCentre = glm::vec3(0.335795f, 0.333385f, 0.193457f);
	leftShoulder = glm::vec3(0.421166f, 0.56706f, 0.1854f); 
	rightShoulder = glm::vec3(0.260455f, 0.567029f, 0.185461f);
	modelPath = mPath;	
	velocity = 0.1f;
	position = glm::vec3(0.0f, 1.0f, 0.0);
	rotation = 0.0f;
	modelRotation = 90.0f;
	aabbSet = false;
	aabb = new AABB();
}

/*
* Loads and creates model/mesh.
*/
void player::init(void)
{
	playerModel = new model(modelPath);
	playerModel->init();
}

/*
* Required to move the player forwards and backwards.
* @param - glm::vec3 - the position of the player.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 player::movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance) 
{
	return glm::vec3(pos.x + distance*std::sin(angle*DEG_TO_RAD), pos.y, pos.z - distance*std::cos(angle*DEG_TO_RAD));
}

/*
* Required to move the player left and right.
* @param - glm::vec3 - the position of the player.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 player::movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance)
{
	return glm::vec3(pos.x + distance*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + distance*std::sin(angle*DEG_TO_RAD));
}

/*
* Moves player forward.
*/
void player::moveForward(float angle)
{
	rotation = angle;
	modelRotation = 180.0f;
	position = movementForward(position, rotation, velocity);	
}

/*
* Moves player backwards.
*/
void player::moveBack(float angle)
{
	rotation = angle;
	modelRotation = 0.0f;
	position = movementForward(position, rotation, -velocity);		
}

/*
* Moves player right.
*/
void player::moveRight(float angle)
{
	rotation = angle;
	modelRotation = 90.0f;
	position = movementRight(position, rotation, velocity);		
}

/*
* Moves player left.
*/
void player::moveLeft(float angle)
{
	rotation = angle;
	modelRotation = 270.0f;
	position = movementRight(position, rotation, -velocity);		
}

/*
* Draws player.
* @param - glm::mat4 - modelview to be used.
* @param - GLuint - shader to be used.
*/
void player::draw(glm::mat4 modelview, GLuint shader)
{
	glUseProgram(shader);
	glCullFace(GL_BACK);

	//Set up identity matrix due to model used. Complete operations
	//to move player to origin before rotation/scale operations and then move back
		
	glm::mat4 leftArm(1.0f);		
	leftArm = glm::translate(leftArm, (leftShoulder));			
	leftArm = glm::rotate(leftArm, -75.0f, glm::vec3(0.0f, 0.0f, 1.0f));	
	leftArm = glm::translate(leftArm, -(leftShoulder));		
	
	glm::mat4 rightArm(1.0f);		
	rightArm = glm::translate(rightArm, (rightShoulder));			
	rightArm = glm::rotate(rightArm, 75.0f, glm::vec3(0.0f, 0.0f, 1.0f));	
	rightArm = glm::translate(rightArm, -(rightShoulder));	

	glm::mat4 mvLeftShoulder(1.0f);		
	mvLeftShoulder = glm::translate(mvLeftShoulder, position);		
	mvLeftShoulder = glm::translate(mvLeftShoulder, modelCentre);	
	mvLeftShoulder = glm::scale(mvLeftShoulder, glm::vec3(10.0f, 10.0f, 10.0f));	
	if(fall == true)
		mvLeftShoulder = glm::rotate(mvLeftShoulder, 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	mvLeftShoulder = glm::rotate(mvLeftShoulder, modelRotation, glm::vec3(0.0f, 1.0f, 0.0f));
	mvLeftShoulder = glm::rotate(mvLeftShoulder, -rotation, glm::vec3(0.0f, 1.0f, 0.0f));	
	mvLeftShoulder = glm::translate(mvLeftShoulder, -modelCentre);
	mvLeftShoulder = mvLeftShoulder * leftArm;
	mvLeftShoulder = modelview * mvLeftShoulder;		
	
	glm::mat4 mvRightShoulder(1.0f);	
	mvRightShoulder = glm::translate(mvRightShoulder, position);		
	mvRightShoulder = glm::translate(mvRightShoulder, modelCentre);	
	mvRightShoulder = glm::scale(mvRightShoulder, glm::vec3(10.0f, 10.0f, 10.0f));	
	if(fall == true)
		mvRightShoulder = glm::rotate(mvRightShoulder, 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	mvRightShoulder = glm::rotate(mvRightShoulder, modelRotation, glm::vec3(0.0f, 1.0f, 0.0f));
	mvRightShoulder = glm::rotate(mvRightShoulder, -rotation, glm::vec3(0.0f, 1.0f, 0.0f));	
	mvRightShoulder = glm::translate(mvRightShoulder, -modelCentre);
	mvRightShoulder = mvRightShoulder * rightArm;
	mvRightShoulder = modelview * mvRightShoulder;	
	
	glm::mat4 MV(1.0f);
	MV = glm::translate(MV, position);
	MV = glm::translate(MV, modelCentre);	
	MV = glm::scale(MV, glm::vec3(10.0f, 10.0f, 10.0f));
	if(fall == true)
		MV = glm::rotate(MV, 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	MV = glm::rotate(MV, modelRotation, glm::vec3(0.0f, 1.0f, 0.0f));
	MV = glm::rotate(MV, -rotation, glm::vec3(0.0f, 1.0f, 0.0f));
	MV = glm::translate(MV, -modelCentre);	
	modelview = modelview * MV;	

	if(aabbSet == false)
	{	
		//Set aabb using model verts.
		std::vector<glm::vec3> tmpVerts = playerModel->getVerts();			
		for(int i = 0; i < tmpVerts.size(); i++)
		{
			glm::vec4 tempVec = MV * glm::vec4(tmpVerts[i].x, tmpVerts[i].y, tmpVerts[i].z, 1.0);
			tmpVerts[i].x = tempVec.x;
			tmpVerts[i].y = tempVec.y;
			tmpVerts[i].z = tempVec.z;	
		}
		aabb->setAABBvec3(tmpVerts);	
		aabbSet = true;		
	}	
		
	playerModel->playerDraw(shader, modelview, mvLeftShoulder, mvRightShoulder);
	glCullFace(GL_FRONT);
}

/*
* Makes player faint by allowing rotation and movement of y axis
* so player is against and facing the ground.
*/
void player::faint(void)
{
	fall = true;
	position.y = -2.0f;
}

/*
* Looks at pos taken in, will update model and movement rotation.
* @param - glm::vec3 - the position to look at.
*/
void player::lookAt(glm::vec3 pos)
{	
	glm::vec3 danteToPos = glm::vec3(pos.x - position.x, 0.0f, pos.z - position.z);	//new look at vector
	glm::vec3 modelLookAt = glm::vec3(0.0f, 0.0f, 1.0f); //model look at vector
	//calculates polar coordinates to find angle player should look at
	float dotModelPos = glm::dot(modelLookAt, danteToPos);
	float modelMag = sqrt(modelLookAt.x*modelLookAt.x + modelLookAt.y*modelLookAt.y + modelLookAt.z*modelLookAt.z);
	float playerToPosMag = sqrt(danteToPos.x*danteToPos.x + danteToPos.y*danteToPos.y + danteToPos.z*danteToPos.z);
	modelRotation = std::acos(dotModelPos/(modelMag*playerToPosMag)) * RAD_TO_DEG;
	if(rotate.y < 0) //change player rotation based on cross to know whether angle is positive or negative
		rotation = -modelRotation + 90;
	else
		rotation = modelRotation + 90;
}