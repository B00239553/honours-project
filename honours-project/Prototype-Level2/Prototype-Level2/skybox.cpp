//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "skybox.h"

/*
* Constructor
*/
skybox::skybox(void)
{
}

/*
* Variable Constructor.
* @param - GLuint - shader program.
* @param - GLuint - mesh to be used.
* @param - GLuint - mesh index count;
*/
skybox::skybox(GLuint shader, GLuint mesh, GLuint count)
{
	skyboxShader = shader;
	meshObject = mesh;
	meshIndexCount = count;
	skyboxScale = glm::vec3(3.0f, 3.0f, 3.0f);
}

/*
* Deconstructor.
*/
skybox::~skybox(void)
{
}

/*
* Loads skybox textures and creates cube map.
*/
void skybox::init(void)
{
	const char *cubeTexFiles[6] = 
	{		
		"hell/hell_front.bmp", "hell/hell_back.bmp",
		"hell/hell_right.bmp", "hell/hell_left.bmp",
		"hell/hell_top.bmp",   "hell/hell_top.bmp"	
	};
	loadCubeMap(cubeTexFiles, &skyboxTexture);	
}

/*
* Loads cube map using files taken in and creates texture.
* @param - char*[] - files to be used for cube map.
* @param - GLuint - texture ID for the cube map.
*/
void skybox::loadCubeMap(const char *fname[6], GLuint *texID)
{
	glGenTextures(1, texID); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
						GL_TEXTURE_CUBE_MAP_POSITIVE_X,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
						GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};
	SDL_Surface *tmpSurface;

	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	for (int i=0;i<6;i++)
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		if (!tmpSurface)		
			std::cout << "Error loading bitmap file " << fname[i] << std::endl;			
		
		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h, 0,
							GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}		
}

/*
* Skybox will be drawn here.
* @param - glm::mat4 - modelview from main draw class.
* @param - glm::mat4 - takes in projection from main draw class.
*/
void skybox::draw(glm::mat4 modelview, glm::mat4 projection)
{		
	glCullFace(GL_FRONT);
	glDepthMask(GL_FALSE); //ensure depth off for drawing skybox
	glUseProgram(skyboxShader);
	rt3d::setUniformMatrix4fv(skyboxShader, "projection", glm::value_ptr(projection));	 
	glm::mat3 mvRotOnlyMat3 = glm::mat3(modelview); //allows skybox to rotate only
	modelview = glm::mat4(mvRotOnlyMat3);		
	glBindTexture(GL_TEXTURE_2D, skyboxTexture);
	modelview = glm::scale(modelview, skyboxScale);
	rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(modelview));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);		
	glCullFace(GL_BACK);
}