//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "game.h"

// Opens console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

/*
* Program entry point. Creates instance of game class that runs prototype.
*/
int main(int argc, char *argv[]) {
	game *prototype = new game();

	prototype->init();
	prototype->runGame();

	delete prototype;
    return 0;    
}