//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#ifndef TEXTURE_OBJECT
#define TEXTURE_OBJECT

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/*
* Texture Object - Texture Library will use this to create texture objects.
*/
class textureObject
{
private:
	std::string name;
	std::string type;
	char * text;
	GLuint width;
	GLuint height;
	glm::vec3 textPosition;
	glm::vec3 textColour;
	GLuint texture;
public:
	textureObject(void);
	~textureObject(void);
	textureObject(std::string mName, std::string mType, char* mText, GLuint mTexture);
	textureObject(std::string mName, std::string mType, char* mText, GLuint mWdith, GLuint mHeight, glm::vec3 pos, glm::vec3 mTextColour, GLuint mTexture);
	std::string getName(void) {return name;}
	std::string getType(void) {return type;}
	char* getText(void) {return text;}
	GLuint getWidth(void) {return width;}
	GLuint getHeight(void) {return height;}
	glm::vec3 getPosition(void) {return textPosition;}
	glm::vec3 getColour(void) {return textColour;}
	GLuint getTexture(void) {return texture;}
};

#endif