//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#ifndef OBJECT_H  
#define OBJECT_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "AABB.h"

/*
* Object class for creating different objects in the world.
*/
class object 
{
private:
	std::string name;	
	std::string type;
	GLuint texture;	
	glm::vec3 translate;
	glm::vec3 rotate;
	glm::vec3 scale;
	float rotation;
	GLuint meshObject;
	int meshIndexCount;		
	AABB * aabb;
public:
	object(void);
	object(std::string mName, std::string mType, GLuint mTex, glm::vec3 mTranslate, glm::vec3 mRotate, float mRotation, glm::vec3 mScale, GLuint mMeshObject, int mMeshCount);	
	~object(void);
	void setAABB(std::vector<GLfloat> verts);
	std::string getName() {return name;}	
	std::string getType() {return type;}
	GLuint getTex(void) {return texture;}	
	glm::vec3 getTranslate(void) {return translate;}
	glm::vec3 getRotate(void) {return rotate;}
	float getRotation(void) {return rotation;}
	glm::vec3 getScale(void) {return scale;}
	GLuint getMeshObject(void) {return meshObject;}
	GLuint getMeshIndexCount(void) {return meshIndexCount;}	
	AABB* getAABB(void) {return aabb;}
	void setTranslate(glm::vec3 pos) {translate = pos;}
	void setRotation(GLfloat rot) {rotation = rot;}	
	void setRotate(glm::vec3 rot) {rotate = rot;}
};

#endif