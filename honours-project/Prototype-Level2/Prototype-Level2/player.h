//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#ifndef PLAYER_H
#define PLAYER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "model.h"
#include "AABB.h"

#define DEG_TO_RAD 0.017453293
#define RAD_TO_DEG 57.29577951f

/*
* Player Class - Will create player, load model and draw player.
*/
class player
{
private:
	bool fall;
	glm::vec3 position;
	glm::vec3 modelCentre;
	glm::vec3 leftShoulder;
	glm::vec3 rightShoulder;
	glm::vec3 rotate;
	GLfloat rotation;
	GLfloat modelRotation;	
	float velocity;
	std::string modelPath;
	model * playerModel;
	AABB * aabb;
	bool aabbSet;
	glm::vec3 movementForward(glm::vec3 pos, GLfloat angle, GLfloat distance);
	glm::vec3 movementRight(glm::vec3 pos, GLfloat angle, GLfloat distance);
public:
	player(void);
	~player(void);
	player(std::string mPath);
	void init(void);	
	glm::vec3 getPos(void) {return position;}
	GLfloat getRot(void) {return rotation;}
	GLfloat getModelRot(void) {return modelRotation;}
	AABB* getAABB(void) {return aabb;}	
	void setPos(glm::vec3 pos) {position = pos;}	
	void moveForward(float angle);
	void moveBack(float angle);
	void moveRight(float angle);
	void moveLeft(float angle);
	void draw(glm::mat4 modelview, GLuint shader);
	void faint(void);
	void lookAt(glm::vec3 pos);
};

#endif PLAYER_H