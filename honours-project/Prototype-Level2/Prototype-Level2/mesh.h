//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <sstream>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#ifndef MESH_H   
#define MESH_H   

/*
* Holds vertex position, normal and texture coordinate.
*/
struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoord;
};

/*
* Holds texture id, the type (e.g. diffuse etc if applicable), and the filepath.
*/
struct Texture
{
	GLuint id;
	std::string type;
	aiString filePath;
};

/*
* Mesh - Holds and draws mesh loaded through model class.
* Based on code from www.learnopengl.com/#!Model-Loading/Mesh.
*/
class mesh 
{
private:
	GLuint VAO, VBO, EBO;	
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	void setUpMesh(void);	
public:		
	mesh(void);
	~mesh(void);
	std::vector<glm::vec3> getVerts(void);
	mesh(std::vector<Vertex> verts, std::vector<GLuint> inds, std::vector<Texture> texs);	
	void draw(GLuint shader);	
};

#endif 