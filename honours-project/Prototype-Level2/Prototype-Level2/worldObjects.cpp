//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "worldObjects.h"


/*
* Constructor.
*/
worldObjects::worldObjects(void)
{
	AABBset = false;
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - STD::vector<GLfloat> - verts of cube, used for AABB creation.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of cube indices.
*/
worldObjects::worldObjects(std::vector<GLfloat> cubeV, GLuint cubeMesh, GLuint cubeCount)
{	
	cubeVerts = cubeV;
	cubeObject = cubeMesh;
	cubeIndex = cubeCount;	
	AABBset = false;
}

/*
* Deconstructor.
*/
worldObjects::~worldObjects(void)
{	
	deleteAllObjects();
}

/*
* Will create textures to be used for objects and create objects for world.
* @param - &textureLibrary - the texture library used to store textures.
*/
void worldObjects::init(textureLibrary &texLib)
{	
	texLib.createImageTexture("ground", "Textures/ground.png");
	texLib.createImageTexture("wall", "Textures/wall.png");
	texLib.createImageTexture("archway", "Textures/archway.png");
	texLib.createImageTexture("signpost", "Textures/signpost.png");
	texLib.createImageTexture("river", "Textures/river.png");
	texLib.createImageTexture("oldBuilding", "Textures/old building.png");
	texLib.createImageTexture("boards", "Textures/boards.png");
	texLib.createImageTexture("ruinedBuilding", "Textures/ruined building.png");

	object *tmpObject = new object("cobbles", "ground", texLib.getTextureObj("ground", "image")->getTexture(), glm::vec3(77.5f, -3.0f, 5.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(175.0f, 1.0f, 150.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("river", "ground", texLib.getTextureObj("river", "image")->getTexture(), glm::vec3(150.0f, -3.0f, 100.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(40.0f, 1.0f, 40.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("wall1", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(-7.5f, 2.5f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 30.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall2", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(0.0f, 2.5f, 12.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(10.0f, 10.0f, 5.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall3", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(0.0f, 2.5f, -12.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(10.0f, 10.0f, 5.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall4", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(7.5f, 2.5f, -40.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 60.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall5", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(7.5f, 2.5f, 42.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 65.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall6", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(87.5f, 2.5f, -67.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(155.0f, 10.0f, 5.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall7", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(75.0f, 2.5f, 77.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(140.0f, 10.0f, 5.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall8", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(162.5f, 2.5f, 7.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 145.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("wall9", "wall", texLib.getTextureObj("wall", "image")->getTexture(), glm::vec3(142.5f, 2.5f, 57.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(5.0f, 10.0f, 35.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("archway1", "archway", texLib.getTextureObj("archway", "image")->getTexture(), glm::vec3(5.01f, 6.0f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 180.0f, glm::vec3(0.0000000000001f, 25.0f, 30.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("archway1", "archway", texLib.getTextureObj("archway", "image")->getTexture(), glm::vec3(152.5f, 6.0f, 75.1f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(25.0f, 25.0f, 0.0000000000001f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("signpost", "signpost", texLib.getTextureObj("signpost", "image")->getTexture(), glm::vec3(43.0f, 0.0f, 0.0f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(0.0000000000001f, 5.0f, 4.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("ruinedBuilding1", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, -57.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding2", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, -22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding3", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, -7.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding1", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, 7.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding2", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, 22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding4", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(52.5f, 17.5f, 37.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	
	tmpObject = new object("ruinedBuilding5", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(67.5f, 17.5f, 37.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("oldBuilding3", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(82.5f, 17.5f, -57.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding4", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(82.5f, 17.5f, -42.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding6", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(82.5f, 17.5f, -27.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding7", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(82.5f, 17.5f, -12.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding5", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(82.5f, 17.5f, 22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);

	tmpObject = new object("oldBuilding6", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, -57.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding7", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, -42.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding8", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, -27.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding9", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, -12.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("ruinedBuilding10", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, 22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("boards1", "building", texLib.getTextureObj("boards", "image")->getTexture(), glm::vec3(97.5f, 5.0f, 37.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("boards2", "building", texLib.getTextureObj("boards", "image")->getTexture(), glm::vec3(97.5f, 5.0f, 52.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding8", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(97.5f, 17.5f, 67.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	
	tmpObject = new object("ruinedBuilding11", "building", texLib.getTextureObj("ruinedBuilding", "image")->getTexture(), glm::vec3(132.5f, 17.5f, -37.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding9", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(132.5f, 17.5f, -22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding10", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(132.5f, 17.5f, -7.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("boards3", "building", texLib.getTextureObj("boards", "image")->getTexture(), glm::vec3(132.5f, 5.0f, 7.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("boards4", "building", texLib.getTextureObj("boards", "image")->getTexture(), glm::vec3(132.5f, 5.0f, 22.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 15.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = new object("oldBuilding11", "building", texLib.getTextureObj("oldBuilding", "image")->getTexture(), glm::vec3(132.5f, 17.5f, 37.5f), 
		glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(15.0f, 40.0f, 15.0f), cubeObject, cubeIndex);
	sceneObjects.push_back(tmpObject);
	tmpObject = NULL;
}

/*
* Deletes all objects held.
*/
void worldObjects::deleteAllObjects(void)
{
	for(int i = 0; i < sceneObjects.size(); i++)
		delete sceneObjects[i];
	sceneObjects.clear();	
}

/*
* Draws all world objects.
* First call will set AABBs for all objects held by calling their set function,
* this is done so AABBs are set after scaling and rotation operations.
* @param - std::stack<glm::mat4> - the current modelview matrix.
* @param - camera - current game camera.
* @param - GLuint - current shader to be used.
*/
void worldObjects::draw(std::stack<glm::mat4> mvStack, camera *gameCam, GLuint shader)	
{
	rt3d::materialStruct materialEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse 
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		2.0f  // shininess
	};
		
	rt3d::materialStruct water = {
		{1.0f, 1.0f, 1.0f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse 
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		2.0f  // shininess
	};

	rt3d::setMaterial(shader, materialEnvironment);
	glm::mat3 normalMatrix(1.0);

	for(int i = 0; i < sceneObjects.size(); i++)
	{		
		if(sceneObjects[i]->getName() == "river")
			rt3d::setMaterial(shader, water);
		glDisable(GL_CULL_FACE);			
		glUseProgram(shader);
		mvStack.push(mvStack.top());		
		glBindTexture(GL_TEXTURE_2D, sceneObjects[i]->getTex());		
		mvStack.top() = glm::translate(mvStack.top(), sceneObjects[i]->getTranslate());	
		if(sceneObjects[i]->getRotation() != 0.0f)
			if(sceneObjects[i]->getRotate() != glm::vec3(0.0f, 0.0f, 0.0f))
				mvStack.top() = glm::rotate(mvStack.top(), sceneObjects[i]->getRotation(), sceneObjects[i]->getRotate());		
		mvStack.top() = glm::scale(mvStack.top(), sceneObjects[i]->getScale());			
		normalMatrix = glm::transpose(glm::inverse(glm::mat3(mvStack.top())));	
		rt3d::setUniformMatrix3fv(shader, "normalmatrix", glm::value_ptr(normalMatrix));		
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(sceneObjects[i]->getMeshObject(),sceneObjects[i]->getMeshIndexCount(),GL_TRIANGLES);
		
		if(sceneObjects[i]->getName() == "river")
			rt3d::setMaterial(shader, materialEnvironment);

		//Sets AABB for world objects, only set once as bool will be true after first call.
		if(AABBset == false)
		{	
			glm::mat4 colMat(1.0);
			colMat = glm::translate(colMat, sceneObjects[i]->getTranslate());	
			if(sceneObjects[i]->getRotate() != glm::vec3(0.0f, 0.0f, 0.0f))
				colMat = glm::rotate(colMat, sceneObjects[i]->getRotation(), sceneObjects[i]->getRotate());		
			colMat = glm::scale(colMat, sceneObjects[i]->getScale());	
			std::vector<GLfloat> tmpVerts;
			tmpVerts = cubeVerts;			
			for(int i = 0; i < tmpVerts.size(); i=i+3)
			{
				glm::vec4 tempVec = colMat * glm::vec4(tmpVerts[i], tmpVerts[i+1], tmpVerts[i+2], 1.0);				
				tmpVerts[i] = tempVec.x;
				tmpVerts[i+1] = tempVec.y;
				tmpVerts[i+2] = tempVec.z;	
			}
			sceneObjects[i]->setAABB(tmpVerts);				
		}		
		mvStack.pop();
	}	
	if(AABBset == false)
		AABBset = true;	
	glEnable(GL_CULL_FACE);
}