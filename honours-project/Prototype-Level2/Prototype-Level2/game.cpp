//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "game.h"
#include "introState.h"
#include "playState.h"
#include "endState.h"

/*
* Constructor.
*/
game::game(void)
{
	gameRunning = true;
}

/*
* Deconstructor.
*/
game::~game(void)
{
	delete texLib;
	delete State_Intro;
	delete State_Play;
	delete State_End;
	delete game_Camera;
	delete game_Player;
	delete game_Virgil;
}

/*
* Initialises SDL and glew.
*/
void game::init(void)
{
	srand (time(NULL));
	hWindow = setupRC(glContext); //Creates window and render context
			
	glewExperimental = GL_TRUE; //Required as used on windows
	GLenum err = glewInit();
	if (GLEW_OK != err) 
	{ 
		std::cout << "glewInit failed, aborting." << std::endl; 
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;		

	//loads shaders here so only required once
	objectShader = rt3d::initShaders("phong-tex.vert", "phong-tex.frag");
	textShader = rt3d::initShaders("text.vert", "text.frag");
	cubeMapShader = rt3d::initShaders("cubeMap.vert", "cubeMap.frag");
		
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
			
	//Load cube object here, and create mesh so only required once.
	std::vector<GLfloat> cubeVerts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;		
	objectLoader::loadObject("cube.txt", cubeVerts, norms, tex_coords, indices);	
	cubeIndexCount = indices.size();	
	cubeMeshObject = rt3d::createMesh(cubeVerts.size()/3, cubeVerts.data(), norms.data(), tex_coords.data(), cubeIndexCount, indices.data(), nullptr);
		
	texLib = new textureLibrary();

	State_Intro = new introState(cubeMeshObject, cubeIndexCount, textShader);
	State_Intro->init(*this);	
	State_Play = new playState(cubeMeshObject, cubeIndexCount, objectShader, textShader);
	State_Play->init(*this);	
	State_End = new endState(cubeMeshObject, cubeIndexCount, textShader);
	State_End->init(*this);
	currentState = State_Intro;	

	sky_Box = new skybox(cubeMapShader, cubeMeshObject, cubeIndexCount);
	sky_Box->init();
	game_Camera = new camera(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	game_Camera->init();
	game_Objects = new worldObjects(cubeVerts, cubeMeshObject, cubeIndexCount);
	game_Objects->init(*texLib);	
	//Paths must be updated if file location changed
	game_Player = new player("C:/Users/Mike/repos/honours-project/Prototype-Level2/Prototype-Level2/Dante/Dante.obj");
	game_Player->init();
	game_Virgil = new virgil("C:/Users/Mike/repos/honours-project/Prototype-Level2/Prototype-Level2/Virgil/Lambent_Male.obj");
	game_Virgil->init();
}

/*
* Sets up rendering context/
* @param - SDL_GLContext - rendering context.
* @return - SDL_Window - window to be drawn to.
*/
SDL_Window * game::setupRC(SDL_GLContext &context)
{
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        std::cout << "Unable to initialize SDL." << std::endl;     
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3); // Request an OpenGL 3.0 context.
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
     
    window = SDL_CreateWindow("Dante Prototype 2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created
        std::cout << "Unable to create window." << std::endl;
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

/*
* Sets new state, it will perform any exit requirements for the current state,
* then sets and calls enter function for new state. 
* @param - gameState - The new game state to be entered.
*/
void game::setState(gameState* newState)
{
	if(newState != currentState)  
	{		
		currentState->exit(*this);
		currentState = newState;	
		currentState->enter(*this);		
	}
}

/*
* Main loop function
*/
void game::runGame(void)
{
	SDL_Event sdlEvent;  // used to detect SDL events
	while (gameRunning)
	{	
		while (SDL_PollEvent(&sdlEvent)) 
		{
			if (sdlEvent.type == SDL_QUIT)
				gameRunning = false;			
		}			
		currentState->draw(hWindow, *this);
		currentState->update(*this);
	}
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();    
}