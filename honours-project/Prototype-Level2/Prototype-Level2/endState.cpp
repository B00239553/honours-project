//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "game.h"
#include "endState.h"

/*
* Constructor.
*/
endState::endState(void)
{
}

/*
* Variable Constructor.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for state.
*/
endState::endState(GLuint mesh, GLuint count, GLuint shader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;
	scaleText = 0.005f;
}

/*
* Deconstructor.
*/
endState::~endState(void)
{
}

/*
* Create text textures. Updates vector to hold texture names
* created. The vector is used to reduce draw function.
* @param - game - the context of the game.
*/
void endState::init(game &context)
{			
	context.getTexLib()->createTextTexture("Title", "Prototype Completed", 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(0, 0, 255));
	context.getTexLib()->createTextTexture("Thanks", "Thank you", 15, glm::vec3(0.0f, 0.3f, 0.0f), glm::vec3(0, 0, 255));
		
	textureNames.push_back("Title");	
	textureNames.push_back("Thanks");		
}

/*
* @param - game - the context of the game.
*/
void endState::enter(game &context)
{		
	std::stringstream str;
	str << "Narrative Found: " << context.getNarrativeFound();
	context.getTexLib()->createTextTexture("narrative", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(255, 0, 0));
	textureNames.push_back("narrative");
}

/*
* Displays the end credits.
* @param - SDL_Window - window to draw to.
* @param - game - game context.
*/
void endState::draw(SDL_Window * window, game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(shaderProgram);

	for(int i = 0; i < textureNames.size(); i++)
	{
		glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj(textureNames[i], "text")->getTexture());	
		mvStack.push(glm::mat4(1.0f));	
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(context.getTexLib()->getTextureObj(textureNames[i], "text")->getPosition()));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(context.getTexLib()->getTextureObj(textureNames[i], "text")->getWidth()*scaleText,
			context.getTexLib()->getTextureObj(textureNames[i], "text")->getHeight()*scaleText, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
	}	

	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - game - the context of the game.
*/
void endState::update(game &context)
{		
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_ESCAPE] )
		context.setGameRunning(false);	
}

/*
* @param - game - the context of the game.
*/
void endState::exit(game &context)
{		
}