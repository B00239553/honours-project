//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "rt3d.h"

namespace rt3d 
{
	/*
	* Will be used inside rt3d library only.
	*/
	struct vaoBuffers
	{
		GLuint vertex_Buffer;
		GLuint normal_Buffer;
		GLuint texCoord_Buffer;
		GLuint index_Buffer;
		GLuint tangent_Buffer;
	};

	//used for creating meshes
	static std::map<GLuint, GLuint *> vertexArrayMap;

	/*
	* Loads file taken in.
	* @param - const char* - name of file.
	* @param - GLint - size of file.
	* @return - char* - file loaded.
	*/
	char* loadFile(const char *fileName, GLint &fileSize)
	{
		int size;
		char *memBlock;

		//Based on an example from www.cplusplus.com
		std::ifstream file(fileName, std::ios::in|std::ios::binary|std::ios::ate);
		if(file.is_open())
		{
			size = (int) file.tellg();
			fileSize = (GLint) size;
			memBlock = new char [size];
			file.seekg(0, std::ios::beg);
			file.read(memBlock, size);
			file.close();
			std::cout << fileName << " loaded successfully." << std::endl;
		}
		else
		{
			std::cout << fileName << " failed to load." << std::endl;
			fileSize = 0;
			return nullptr;
		}
		return memBlock;
	}

	/*
	* Takes in shader file to check error that occurred.
	* @param - GLint - shader file to find error in.
	*/
	void shaderError(const GLint shader)
	{
		int maxLength = 0;
		int logLength = 0;
		GLchar * logMessage;

		if(!glIsShader(shader))
			glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
		else
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		if(maxLength > 0)
		{
			logMessage = new GLchar[maxLength];
			if(!glIsShader(shader))
				glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
			else
				glGetShaderInfoLog(shader, maxLength, &logLength, logMessage);
			std::cout << "Shader Info Log " << std::endl << logMessage << std::endl;
			delete [] logMessage;
		}
	}

	/*
	* Initialises shaders, vert and frag files.
	* @param - const char* - vert shader name.
	* @param - const char* - frag file name.
	* @return - GLuint - location of shader loaded in.
	*/
	GLuint initShaders(const char *vertFile, const char *fragFile)
	{
		GLuint p, frag, vert;
		char *vs, *fs;
		vert = glCreateShader(GL_VERTEX_SHADER);
		frag = glCreateShader(GL_FRAGMENT_SHADER);

		GLint vertLen;
		GLint fragLen;
		vs = loadFile(vertFile, vertLen);
		fs = loadFile(fragFile, fragLen);

		const char * vv = vs;
		const char * ff = fs;

		glShaderSource(vert, 1, &vv, &vertLen);
		glShaderSource(frag, 1, &ff, &fragLen);

		GLint compiled;

		glCompileShader(vert);
		glGetShaderiv(vert, GL_COMPILE_STATUS, &compiled);
		if(!compiled)
		{
			std::cout << "Vertex shader not compiled." << std::endl;
			rt3d::shaderError(vert);
		}
		

		glCompileShader(frag);
		glGetShaderiv(frag, GL_COMPILE_STATUS, &compiled);
		if(!compiled)
		{
			std::cout << "Fragment shader not compiled." << std::endl;
			rt3d::shaderError(frag);
		}

		p = glCreateProgram();

		glAttachShader(p, vert);
		glAttachShader(p, frag);

		glLinkProgram(p);
		glUseProgram(p);

		//Free memory used in load file function.
		delete [] vs;
		delete [] fs;

		return p;
	}

	/*
	* Sets 3x3 matrix for shader taken in.
	* @param - name of shader program.
	* @param - name of shader variable to be set.
	* @param - the data for the shader variable.
	*/
	void setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data)
	{
		int uniformIndex = glGetUniformLocation(program, uniformName);
		glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, data);
	}

	/*
	* Sets 4x4 matrix for shader taken in.
	* @param - name of shader program.
	* @param - name of shader variable to be set.
	* @param - the data for the shader variable.
	*/
	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data)
	{
		int uniformIndex = glGetUniformLocation(program, uniformName);
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data);
	}

	/*
	* Sets 3 component vector for shader taken in.
	* @param - name of shader program.
	* @param - name of shader variable to be set.
	* @param - the data for the shader variable.
	*/
	void setUniformVector3fv(const GLuint program, const char* uniformName, const GLfloat *data)
	{
		int uniformIndex = glGetUniformLocation(program, uniformName);
		glUniform3fv(uniformIndex, 1, data);
	}

	/*
	* Sets 4 component vector for shader taken in.
	* @param - name of shader program.
	* @param - name of shader variable to be set.
	* @param - the data for the shader variable.
	*/
	void setUniformVector4fv(const GLuint program, const char* uniformName, const GLfloat *data)
	{
		int uniformIndex = glGetUniformLocation(program, uniformName);
		glUniform4fv(uniformIndex, 1, data);
	}

	/*
	* Sets light struct for shader taken in.
	* @param - program - name of shader program.
	* @param - lightStruct - light to be set.
	*/
	void setLight(const GLuint program, const lightStruct light)
	{
		int uniformIndex = glGetUniformLocation(program, "light.ambient");
		glUniform4fv(uniformIndex, 1, light.ambient);
		uniformIndex = glGetUniformLocation(program, "light.diffuse");
		glUniform4fv(uniformIndex, 1, light.diffuse);
		uniformIndex = glGetUniformLocation(program, "light.specular");
		glUniform4fv(uniformIndex, 1, light.specular);			
		uniformIndex = glGetUniformLocation(program, "spotCosCutOff");
		glUniform1f(uniformIndex, light.spotCosCutOff);
		uniformIndex = glGetUniformLocation(program, "spotExponent");
		glUniform1f(uniformIndex, light.spotExponent);
	}

	/*
	* Sets material struct for shader taken in.
	* @param - program - name of shader program.
	* @param - materialStruct - material to be set.
	*/
	void setMaterial(const GLuint program, const materialStruct material)
	{
		int uniformIndex = glGetUniformLocation(program, "material.ambient");
		glUniform4fv(uniformIndex, 1, material.ambient);
		uniformIndex = glGetUniformLocation(program, "material.diffuse");
		glUniform4fv(uniformIndex, 1, material.diffuse);
		uniformIndex = glGetUniformLocation(program, "material.specular");
		glUniform4fv(uniformIndex, 1, material.specular);
		uniformIndex = glGetUniformLocation(program, "material.shininess");
		glUniform1f(uniformIndex, material.shininess);
	}

	/*
	* Creates mesh using data taken in.
	* @param - GLuint - number of vertices.
	* @param - GLfloat* - vertices.
	* @param - GLfloat* - normals.
	* @param - GLfloat* - texture coordinates.
	* @param - GLint - index count.
	* @param - GLfloat* - indices.
	* @param - GLfloat* - tangents.
	* @return - GLuint - the identifier needed to draw this mesh.
	*/
	GLuint createMesh(const GLuint numVerts, const GLfloat *vertices, const GLfloat *normals, const GLfloat *texCoords, 
		const GLuint indexCount, const GLuint * indices, const GLfloat *tangents)
	{
		// generate and set up a VAO for the mesh
		GLuint VAO;		
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		GLuint *pMeshBuffers = new GLuint[5];
		
		if (vertices == nullptr)
		{
			//Cant create mesh if no vertices
			std::cout << "Attempt to create a mesh with no vertices." << std::endl;
		}

		// generate and set up the VBOs for the data
		GLuint VBO;
		glGenBuffers(1, &VBO);
	
		// VBO for vertex data
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), vertices, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)RT3D_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0); 
		glEnableVertexAttribArray(RT3D_VERTEX);
		pMeshBuffers[RT3D_VERTEX] = VBO;
		
		// VBO for normal data
		if (normals != nullptr) 
		{
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), normals, GL_STATIC_DRAW);
			glVertexAttribPointer((GLuint)RT3D_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(RT3D_NORMAL);
			pMeshBuffers[RT3D_NORMAL] = VBO;
		}

		// VBO for tex-coord data
		if (texCoords != nullptr)
		{
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, 2*numVerts*sizeof(GLfloat), texCoords, GL_STATIC_DRAW);
			glVertexAttribPointer((GLuint)RT3D_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(RT3D_TEXCOORD);
			pMeshBuffers[RT3D_TEXCOORD] = VBO;
		}

		if (indices != nullptr && indexCount > 0) 
		{
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), indices, GL_STATIC_DRAW);
			pMeshBuffers[RT3D_INDEX] = VBO;
		}

		// VBO for tangent data
		if (tangents != nullptr) 
		{
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, 4*numVerts*sizeof(GLfloat), tangents, GL_STATIC_DRAW);
			glVertexAttribPointer((GLuint)RT3D_TANGENT, 4, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(RT3D_TANGENT);
			pMeshBuffers[RT3D_TANGENT] = VBO;
		}

		// unbind vertex array
		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		vertexArrayMap.insert( std::pair<GLuint, GLuint *>(VAO, pMeshBuffers) );
				
		return VAO;
	}

	/*
	* Updates a mesh for animations.
	* @param - GLuint - the mesh to be updated.
	* @param - unsigned int - vertices.
	* @param - GLfloat* - the animation vertices.
	* @param - GLuint - the size of the vertices data.
	*/
	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size)
	{
		GLuint * pMeshBuffers = vertexArrayMap[mesh];
		glBindVertexArray(mesh);

		// Delete the old buffer data
		glDeleteBuffers(1, &pMeshBuffers[bufferType]);

		// generate and set up the VBOs for the new data
		GLuint VBO;
		glGenBuffers(1, &VBO);
		// VBO for the data
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, size*sizeof(GLfloat), data, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)bufferType, 3, GL_FLOAT, GL_FALSE, 0, 0); 
		glEnableVertexAttribArray(bufferType);
		pMeshBuffers[RT3D_VERTEX] = VBO;

		glBindVertexArray(0);
	}

	/*
	* Draws an indexed mesh.
	* @param - GLuint - the mesh to be drawn
	* @param - GLuint - the indexCount of the mesh.
	* @param - GLuint - the primitive type to be drawn.
	*/
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive)
	{
		glBindVertexArray(mesh);	// Bind mesh VAO
		glDrawElements(primitive, indexCount,  GL_UNSIGNED_INT, 0);	// draw VAO 
		glBindVertexArray(0);
	}
}