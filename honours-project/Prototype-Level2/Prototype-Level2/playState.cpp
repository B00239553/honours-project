//(c) Michael O'Neil 2015
//Dante Prototype - Level 2

#include "game.h"
#include "playState.h"
#include <sstream>

/*
* Constructor.
*/
playState::playState(void)
{
}

/*
* Variable Constructor.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for objects.
* @param - GLuint - shader program for HUD.
*/
playState::playState(GLuint mesh, GLuint count, GLuint objShader, GLuint hShader)
{	
	narrativeFound = 0;	
	lockControls = false;
	narrativeOne = false;
	narrativeTwo = false;
	narrativeThree = false;
	narrativeFour = false;
	narrativeFive = false;
	narrativePos = 0;
	narrativePlaying = 0;
	narrativeTimer = clock();
	keyTimer = clock();
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = objShader;
	hudShader = hShader;	
	attenuation = glm::vec3(1.0f, 0.0f, 0.005f);
}

/*
* Deconstructor.
*/
playState::~playState(void)
{
}

/*
* 
* @param - game - the context of the game.
*/
void playState::init(game &context)
{			
	//narrative textures
	context.getTexLib()->createTextTexture("dante1.1", "Dante: \"Where are we?\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante1.1");
	context.getTexLib()->createTextTexture("virgil1.1", "Virgil: \"We're at the entrance to hell. We must find the river,\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil1.1");
	context.getTexLib()->createTextTexture("virgil1.2", "Virgil: \"it is the only passage to reach the nine circles of hell.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil1.2");
	context.getTexLib()->createTextTexture("dante2.1", "Dante: \"Poet, you who are my guide, why would pure Beatrice send you?\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante2.1");
	context.getTexLib()->createTextTexture("virgil2.1", "Virgil: \"The lady so blessed and beautiful came to me, my apt speech she said,\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.1");
	context.getTexLib()->createTextTexture("virgil2.2", "Virgil: \"would aid your passage by those who intend to block it. And for my \"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.2");
	context.getTexLib()->createTextTexture("virgil2.3", "Virgil: \"troubles, she will speak kindly of me for when she bows before her \"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.3");
	context.getTexLib()->createTextTexture("virgil2.4", "Virgil: \"master in heaven once more, so I may leave my damnation.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.4");
	context.getTexLib()->createTextTexture("dante2.2", "Dante: \"Why would my love, safe in heaven, risk entering this place?\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante2.2");
	context.getTexLib()->createTextTexture("virgil2.5", "Virgil: \"She has seen you turn from the common crowd due to your love for her,\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.5");
	context.getTexLib()->createTextTexture("virgil2.6", "Virgil: \"your actions that day, set you on the path that led to your curse.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.6");
	context.getTexLib()->createTextTexture("virgil2.7", "Virgil: \"For this, she would risk everything to set you on your way to her.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil2.7");
	context.getTexLib()->createTextTexture("dante3.1", "Dante: \"Your face is pale. How shall I come, if you are so afraid,\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante3.1");
	context.getTexLib()->createTextTexture("dante3.2", "Dante: \"you who give comfort to me when I waver.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante3.2");
	context.getTexLib()->createTextTexture("virgil3.1", "Virgil: \"It is the anguish of those across the river which take the colour \"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil3.1");
	context.getTexLib()->createTextTexture("virgil3.2", "Virgil: \"from my face. It is pity although you take it for fear.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil3.2");
	context.getTexLib()->createTextTexture("virgil3.3", "Virgil: \"Let's go, we must not stop.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil3.3");
	context.getTexLib()->createTextTexture("virgil4.1", "Virgil: \"Before we proceed, you must be warned. \"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil4.1");
	context.getTexLib()->createTextTexture("virgil4.2", "Virgil: \"They will know you are a living soul, be careful who you talk to, \"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil4.2");
	context.getTexLib()->createTextTexture("virgil4.3", "Virgil: \"most will try to trick you in this dark place.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil4.3");
	context.getTexLib()->createTextTexture("dante4.1", "Dante: \"How will I pass them with the fear I have?\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante4.1");
	context.getTexLib()->createTextTexture("virgil4.4", "Virgil: \"Stay close to me and I will not let them drain you.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil4.4");
	context.getTexLib()->createTextTexture("dante5.1", "Dante: \"We've made it to the river but how shall we cross\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante5.1");
	context.getTexLib()->createTextTexture("virgil5.1", "Virgil: \"Charon will come, he will not block us as this is willed.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil5.1");
	context.getTexLib()->createTextTexture("virgil5.2", "Virgil: \"But do not annoy him, no one can enter that Charon will not have.\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("virgil5.2");	
	context.getTexLib()->createTextTexture("dante5.2", "Dante: \"Wait what's that sound!?!\"", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 0, 0));
	narrativeNames.push_back("dante5.2");
	context.getTexLib()->createTextTexture("earthquake", "*EARTHQUAKE*", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("earthquake");
	context.getTexLib()->createTextTexture("faints", "*Dante faints*", 18, glm::vec3(0.0f, -0.8f, 0.0f), glm::vec3(255, 100, 0));
	narrativeNames.push_back("faints");

	//HUD texture
	std::stringstream str;
	str << "Narrative Found: " << narrativeFound << "/4";	
	context.getTexLib()->createTextTexture("narrativeCount", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));	
}

/*
* @param - game - the context of the game.
*/
void playState::enter(game &context)
{			
		
}

/*
* 
* @param - SDL_Window - window to draw to.
* @param - game - game context.
*/
void playState::draw(SDL_Window * window, game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
		
	rt3d::lightStruct lightEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{3.0f, 3.0f, 3.0f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular		
		std::acos(90.0f),         // spotCosCutOff
		std::acos(100.0f)         // spotExponent
	};
	
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);	
	glm::mat4 modelview(1.0); // set base position for scene	
	glm::mat3 normalMatrix(1.0); //used for normal transformation

	//Set camera
	mvStack.push(modelview);	
	context.getGameCamera()->update(context.getGamePlayer()->getPos());
	if(narrativePlaying == 6)
		context.getGameCamera()->shake();
	mvStack.top() = glm::lookAt(context.getGameCamera()->getEye(), context.getGameCamera()->getAt(), context.getGameCamera()->getUp());
	
	//Draws skybox
	context.getSkyBox()->draw(mvStack.top(), projection);

	glDepthMask(GL_TRUE); // ensure depth test back on after skybox
			
	glUseProgram(shaderProgram);

	//sets light
	rt3d::setLight(shaderProgram, lightEnvironment);

	//set light position, coneDirection and attenuation
	lightPos = glm::vec4(context.getGameCamera()->getEye().x, context.getGameCamera()->getEye().y, context.getGameCamera()->getEye().z, 1.0f);
	glm::vec4 tmpLight = mvStack.top() * lightPos;
	rt3d::setUniformVector4fv(shaderProgram, "lightPosition", glm::value_ptr(tmpLight));
	coneDirection = glm::vec4(context.getGameCamera()->getAt().x, context.getGameCamera()->getAt().y, context.getGameCamera()->getAt().z, 1.0f);
	glm::vec4 tmpConeDirection = mvStack.top() * coneDirection;
	rt3d::setUniformVector4fv(shaderProgram, "coneDirection", glm::value_ptr(tmpConeDirection));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
		
	//set projection
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));	
	
	//draw objects
	context.getGameObjects()->draw(mvStack, context.getGameCamera(), shaderProgram);

	context.getGamePlayer()->draw(mvStack.top(), shaderProgram);
			
	context.getGameVirgil()->draw(mvStack.top(), shaderProgram);

	mvStack.pop(); //last pop

	//Draw HUD			
	glDepthMask(GL_FALSE);
	glUseProgram(hudShader);
	rt3d::setUniformMatrix4fv(hudShader, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj("narrativeCount", "text")->getTexture());	
	
	glm::mat4 hudMV(1.0f);	
	hudMV = glm::translate(hudMV, glm::vec3(-0.6f, 0.95f, 0.0f));
	hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj("narrativeCount", "text")->getWidth()*0.003f,
		context.getTexLib()->getTextureObj("narrativeCount", "text")->getHeight()*0.004f, 0.0f));	
	hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
	rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		
	//If narrative playing
	if(narrativePlaying > 0)
	{
		if(narrativePlaying == 1 && narrativePos >= 3)//exits narrative in narrativePos is at end of current narrative
		{
			lockControls = false;
			narrativePlaying = 0;
			narrativePos -= 1;
			narrativeFound++;
			context.getTexLib()->deleteTexture("narrativeCount", "text"); //deleting so can recreate with updated amount
			std::stringstream str;
			str << "Narrative Found: " << narrativeFound << "/4";	
			context.getTexLib()->createTextTexture("narrativeCount", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
		}
		if(narrativePlaying == 2 && narrativePos >= 12)
		{
			lockControls = false;
			narrativePlaying = 0;
			narrativePos -= 1;
			narrativeFound++;
			context.getTexLib()->deleteTexture("narrativeCount", "text");
			std::stringstream str;
			str << "Narrative Found: " << narrativeFound << "/4";	
			context.getTexLib()->createTextTexture("narrativeCount", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
		}
		if(narrativePlaying == 3 && narrativePos >= 17)
		{
			lockControls = false;
			narrativePlaying = 0;
			narrativePos -= 1;
			narrativeFound++;
			context.getTexLib()->deleteTexture("narrativeCount", "text");
			std::stringstream str;
			str << "Narrative Found: " << narrativeFound << "/4";	
			context.getTexLib()->createTextTexture("narrativeCount", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
		}
		if(narrativePlaying == 4 && narrativePos >= 22)
		{
			lockControls = false;
			narrativePlaying = 0;
			narrativePos -= 1;
			narrativeFound++;
			context.getTexLib()->deleteTexture("narrativeCount", "text");
			std::stringstream str;
			str << "Narrative Found: " << narrativeFound << "/4";	
			context.getTexLib()->createTextTexture("narrativeCount", const_cast<char*>(str.str().c_str()), 24, glm::vec3(0.0f, 0.6f, 0.0f), glm::vec3(255, 0, 0));
		}
		if(narrativePlaying == 5 && narrativePos >= 25)
		{			
			narrativePlaying++;
			narrativePos++;				
		}
		if(narrativePlaying == 6 && clock() > narrativeTimer)
		{
			narrativePlaying++;
			narrativePos++;	
			narrativeTimer = clock() + 8000;			
		}
		if(narrativePlaying == 7)
		{
			context.getGamePlayer()->faint();
			if(clock() > narrativeTimer)
			{
				context.setNarrativeFound(narrativeFound);
				context.setState(context.getEndState());
			}
		}
		if(clock() < narrativeTimer)//draw current narrative
		{
			hudMV = glm::mat4(1.0f);
			glBindTexture(GL_TEXTURE_2D, context.getTexLib()->getTextureObj(narrativeNames[narrativePos], "text")->getTexture());	
			hudMV = glm::translate(hudMV, context.getTexLib()->getTextureObj(narrativeNames[narrativePos], "text")->getPosition());
			hudMV = glm::scale(hudMV,glm::vec3(context.getTexLib()->getTextureObj(narrativeNames[narrativePos], "text")->getWidth()*0.003f,
			context.getTexLib()->getTextureObj(narrativeNames[narrativePos], "text")->getHeight()*0.004f, 0.0f));	
			hudMV = glm::rotate(hudMV, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));			
			rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(hudMV));	
			rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		}
		else//current narrative time has passed, update pos in narrative vector to next narrative
		{
			//move on to next narrative speech
			narrativeTimer = clock() + 8000;
			if(narrativePos < narrativeNames.size())
				narrativePos++;			
		}			
	}	
	
	glDepthMask(GL_TRUE);

	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - game - the context of the game.
*/
void playState::update(game &context)
{		
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	
	//player controls
	//moves player, if collision occurs, moves player in reverse direction
	//from what key was pressed.
	if(lockControls == false)
	{
		if ( keys[SDL_SCANCODE_W] )
		{
			context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_S] )
		{
			context.getGamePlayer()->moveBack(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveForward(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_A] )
		{
			context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
		}
		if ( keys[SDL_SCANCODE_D] ) 
		{
			context.getGamePlayer()->moveRight(context.getGameCamera()->getRotation());		
			context.getGamePlayer()->getAABB()->updateAABB(context.getGamePlayer()->getPos());
			bool collision = playerObjectCollision(context);
			if(collision == true)		
				context.getGamePlayer()->moveLeft(context.getGameCamera()->getRotation());		
		}
	}
	//camera controls
	//rotates camera, if collision occurs, rotates reverse direction
	//from what key was pressed.
	if ( keys[SDL_SCANCODE_Q] || keys[SDL_SCANCODE_LEFT] )
	{
		context.getGameCamera()->rotateLeft();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateRight();	
	}
	if ( keys[SDL_SCANCODE_E] || keys[SDL_SCANCODE_RIGHT] ) 
	{
		context.getGameCamera()->rotateRight();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->rotateLeft();				
	}
	if ( keys[SDL_SCANCODE_R] || keys[SDL_SCANCODE_UP] )
	{	
		context.getGameCamera()->lookUp();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookDown();			
				
	}
	if ( keys[SDL_SCANCODE_F] || keys[SDL_SCANCODE_DOWN] ) 
	{
		context.getGameCamera()->lookDown();
		context.getGameCamera()->update(context.getGamePlayer()->getPos());
		bool collision = cameraObjectCollision(context);
		if(collision == true)		
			context.getGameCamera()->lookUp();				
	}
	if ( keys[SDL_SCANCODE_SPACE] && clock() > keyTimer)
	{
		keyTimer = clock() + 1000;
		if(narrativePos < 25)
		{
			narrativePos++;
			narrativeTimer = clock() + 8000;	
		}
	}

	//Make Virgil follow player
	context.getGameVirgil()->seek(*context.getGamePlayer()->getAABB());	

	//Sets start up for narrative one, only called if narrative one has not been played before
	//inside the if statement
	if(narrativeOne == false && playerNarrativeOneCollision(context) == true)
	{
		narrativePlaying = 1;
		lockControls = true;
		narrativeTimer = clock() + 8000;
		narrativeOne = true;
		narrativePos = 0;		
		context.getGamePlayer()->lookAt(context.getGameVirgil()->getPos());
		context.getGameCamera()->setRotation(-context.getGamePlayer()->getModelRot());		
	}
	if(narrativeTwo == false && playerNarrativeTwoCollision(context) == true)
	{
		narrativePlaying = 2;
		lockControls = true;
		narrativeTimer = clock() + 8000;
		narrativeTwo = true;
		narrativePos = 3;	
		context.getGamePlayer()->lookAt(context.getGameVirgil()->getPos());
		context.getGameCamera()->setRotation(-context.getGamePlayer()->getModelRot());
	}
	if(narrativeThree == false && playerNarrativeThreeCollision(context) == true)
	{		
		narrativePlaying = 3;
		lockControls = true;
		narrativeTimer = clock() + 8000;
		narrativeThree = true;
		narrativePos = 12;		
		context.getGamePlayer()->lookAt(context.getGameVirgil()->getPos());
		context.getGameCamera()->setRotation(-context.getGamePlayer()->getModelRot());
	}
	if(narrativeFour == false && playerNarrativeFourCollision(context) == true)
	{
		narrativePlaying = 4;
		lockControls = true;
		narrativeTimer = clock() + 8000;
		narrativeFour = true;
		narrativePos = 17;	
		context.getGamePlayer()->lookAt(context.getGameVirgil()->getPos());
		context.getGameCamera()->setRotation(-context.getGamePlayer()->getModelRot());
	}
	if(narrativeFive == false && playerNarrativeFiveCollision(context) == true)
	{
		narrativePlaying = 5;
		lockControls = true;
		narrativeTimer = clock() + 8000;
		narrativeFive = true;
		narrativePos = 22;		
		context.getGamePlayer()->lookAt(context.getGameVirgil()->getPos());
		context.getGameCamera()->setRotation(-context.getGamePlayer()->getModelRot());		
	}
}

/*
* Returns if player has collided with an object - current ignores billboards and ground.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::playerObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "ground" && context.getGameObjects()->getObject(i)->getType() != "archway")
			if(context.getGamePlayer()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)
				return true;		
	return false;		
}

/*
* Returns if camera has collided with an object - current ignores billboards.
* @param - game - game context.
* @return - bool - result of collision.
*/
bool playState::cameraObjectCollision(game &context)
{
	for(int i = 0; i < context.getGameObjects()->getNumObjects(); i++)	
		if(context.getGameObjects()->getObject(i)->getType() != "archway")
			if(context.getGameCamera()->getAABB()->AABBcollision(*context.getGameObjects()->getObject(i)->getAABB()) == true)			
				return true;		
	return false;
}

/*
* Returns bool if player is narrative boundary.
* @param - game - the game context.
* @return - bool - returns false if player not in boundary, true otherwises
*/
bool playState::playerNarrativeOneCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 15.0f && context.getGamePlayer()->getPos().x < 40.0f
		&& context.getGamePlayer()->getPos().z > -10.0f && context.getGamePlayer()->getPos().z < 10.0f)
			return true;
	return false;
}

/*
* Returns bool if player is narrative boundary.
* @param - game - the game context.
* @return - bool - returns false if player not in boundary, true otherwises
*/
bool playState::playerNarrativeTwoCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 60.0f && context.getGamePlayer()->getPos().x < 80.0f
		&& context.getGamePlayer()->getPos().z > 50.0f && context.getGamePlayer()->getPos().z < 70.0f)
			return true;
	return false;
}
	
/*
* Returns bool if player is narrative boundary.
* @param - game - the game context.
* @return - bool - returns false if player not in boundary, true otherwises
*/
bool playState::playerNarrativeThreeCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 100.0f && context.getGamePlayer()->getPos().x < 130.0f
		&& context.getGamePlayer()->getPos().z > 50.0f && context.getGamePlayer()->getPos().z < 70.0f)
			return true;
	return false;
}
	
/*
* Returns bool if player is narrative boundary.
* @param - game - the game context.
* @return - bool - returns false if player not in boundary, true otherwises
*/
bool playState::playerNarrativeFourCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 140.0f && context.getGamePlayer()->getPos().x < 160.0f
		&& context.getGamePlayer()->getPos().z > -50.0f && context.getGamePlayer()->getPos().z < -30.0f)
			return true;
	return false;
}

/*
* Returns bool if player is narrative boundary.
* @param - game - the game context.
* @return - bool - returns false if player not in boundary, true otherwises
*/
bool playState::playerNarrativeFiveCollision(game &context)
{
	if(context.getGamePlayer()->getPos().x > 140.0f && context.getGamePlayer()->getPos().x < 160.0f
		&& context.getGamePlayer()->getPos().z > 60.0f && context.getGamePlayer()->getPos().z < 80.0f)
			return true;
	return false;
}

/*
* @param - game - the context of the game.
*/
void playState::exit(game &context)
{		
}